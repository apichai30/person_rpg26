<?php
session_start();
include('header.php');
// include('slide.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';

//$sql = "SELECT *FROM tbl_news INNER JOIN tbl_newstype ON tbl_news.newstype_id = tbl_newstype.newstype_id ORDER BY news_id DESC";
//$res_news = mysqli_query($dbcon, $sql);

// ดึงข้อมูลจาก newstype_id
$newstype_id_001 = "SELECT * FROM tbl_news WHERE newstype_id=001 ORDER BY news_id DESC LIMIT 3";
$res_news_001 = mysqli_query($dbcon, $newstype_id_001);

$newstype_id_002 = "SELECT * FROM tbl_news WHERE newstype_id=002 ORDER BY news_id DESC LIMIT 3";
$res_news_002 = mysqli_query($dbcon, $newstype_id_002);

$newstype_id_003 = "SELECT * FROM tbl_news WHERE newstype_id=003 ORDER BY news_id DESC LIMIT 3";
$res_news_003 = mysqli_query($dbcon, $newstype_id_003);

$newstype_id_004 = "SELECT * FROM tbl_news WHERE newstype_id=004 ORDER BY news_id DESC LIMIT 3";
$res_news_004 = mysqli_query($dbcon, $newstype_id_004);

$newstype_id_005 = "SELECT * FROM tbl_news WHERE newstype_id=005 ORDER BY news_id DESC LIMIT 3";
$res_news_005 = mysqli_query($dbcon, $newstype_id_005);

//ดึงข้อมูลจาก news_status
$news_status = "SELECT * FROM tbl_news WHERE news_status=1 ORDER BY news_id DESC LIMIT 3";
$res_news_status = mysqli_query($dbcon, $news_status);

//echo print_r($res_news_status); //เช็คค่า array ที่ส่งมา
?>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <!-- บอกตำแหน่งที่อยู่ -->
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
          <li class="breadcrumb-item active" aria-current="page">ข่าวสารประชาสัมพันธ์</li>
        </ol>
      </nav>
      <!-- บอกตำแหน่งที่อยู่ -->
      <!-- ข่าวเด่น -->
      <div class="col-xs-12">
        <h3> ข่าวเด่น
          <a class="btn btn-primary btn-xs" href="show_by_newstype.php?id=10">ดูทั้งหมด</a>
        </h3>
        <hr style="border: 1px solid #d6d4d4;">
        <?php
        while ($row_news_status = mysqli_fetch_assoc($res_news_status)) {
          ?>
          <div class="row">
            <div class="col-md-4" align="center">
              <img src="news_image/<?php echo $row_news_status['news_filename']; ?>" style="width:40%;height:auto;">
            </div>
            <div class="col-md-8"><br>
              <p> <b> <?php echo $row_news_status['news_topic']; ?> </b></p> <!-- หัวข้อข่าว -->
              <p> <b>เขียนโดย : </b><?php echo $row_news_status['news_post_by']; ?></p>
              <p> <b>วันเวลาที่เขียน : </b><?php echo date('d/m/Y H:i:s', strtotime($row_news_status['news_date'])); ?> </p>
              <br />
              <a href="view_news.php?id=<?= $row_news_status['news_id']; ?>" class="btn btn-info btn-xs">
                <span class="glyphicon glyphicon-search"> </span> อ่านเพิ่มเติม </a>
            </div>
          </div>
          <hr style="border: 1px solid #d6d4d4;">
        <?php
      }
      ?>
      </div>
      <div class="col-xs-12"><br></div>
      <!-- ข่าวสารทั่วไป -->
      <div class="col-lg-6">
        <h3> ข่าวสารที่น่าสนใจ
          <a class="btn btn-primary btn-xs" href="show_by_newstype.php?id=001">ดูทั้งหมด</a>
        </h3>
        <hr style="border: 1px solid #d6d4d4;">
        <?php
        while ($row_news_001 = mysqli_fetch_assoc($res_news_001)) {
          ?>
          <div class="col-md-4" align="center">
            <img src="news_image/<?php echo $row_news_001['news_filename']; ?>" style="width:80%;height:auto;">
          </div>
          <div class="col-md-8" style="width:60%;height:auto;"><br>
            <p><b> <?php echo $row_news_001['news_topic']; ?> </b></p> <!-- หัวข้อข่าว -->
            <p><b>เขียนโดย : </b><?php echo $row_news_001['news_post_by']; ?></p>
            <p><b>วันเวลาที่เขียน : </b><?php echo date('d/m/Y H:i:s', strtotime($row_news_001['news_date'])); ?></p>

            <a href="view_news.php?id=<?= $row_news_001['news_id']; ?>" class="btn btn-info btn-xs">
              <span class="glyphicon glyphicon-search"> </span> อ่านเพิ่มเติม </a>
            <hr style="border: 1px solid #d6d4d4;">
          </div>
        <?php
      }
      ?>
      </div>
      <!-- ข่าวสารแจ้งการประชุม -->
      <div class="col-lg-6">
        <h3> ข่าวสารแจ้งการประชุม
          <a class="btn btn-primary btn-xs" href="show_by_newstype.php?id=002">ดูทั้งหมด</a>
        </h3>
        <hr style="border: 1px solid #d6d4d4;">
        <?php
        while ($row_news_002 = mysqli_fetch_assoc($res_news_002)) {
          ?>
          <div class="col-md-4" align="center">
            <img src="news_image/<?php echo $row_news_002['news_filename']; ?>" style="width:80%;height:auto;">
          </div>
          <div class="col-md-8" style="width:60%;height:auto;"><br>
            <p><b> <?php echo $row_news_002['news_topic']; ?> </b></p> <!-- หัวข้อข่าว -->
            <p> <b>เขียนโดย : </b><?php echo $row_news_002['news_post_by']; ?></p>
            <p> <b>วันเวลาที่เขียน : </b><?php echo date('d/m/Y H:i:s', strtotime($row_news_002['news_date'])); ?> </p>

            <a href="view_news.php?id=<?= $row_news_002['news_id']; ?>" class="btn btn-info btn-xs">
              <span class="glyphicon glyphicon-search"> </span> อ่านเพิ่มเติม </a>
            <hr style="border: 1px solid #d6d4d4;">
          </div>
        <?php
      }
      ?>
      </div>
      <div class="col-xs-12"><br></div>
      <!-- ข่าวสารแจ้งกิจกรรม -->
      <div class="col-lg-6">
        <h3> ข่าวสารแจ้งกิจกรรม
          <a class="btn btn-primary btn-xs" href="show_by_newstype.php?id=003">ดูทั้งหมด</a>
        </h3>
        <hr style="border: 1px solid #d6d4d4;">
        <?php
        while ($row_news_003 = mysqli_fetch_assoc($res_news_003)) {
          ?>
          <div class="col-md-4" align="center">
            <img src="news_image/<?php echo $row_news_003['news_filename']; ?>" style="width:80%;height:auto;">
          </div>
          <div class="col-md-8" style="width:60%;height:auto;"><br>
            <p><b> <?php echo $row_news_003['news_topic']; ?> </b></p> <!-- หัวข้อข่าว -->
            <p> <b>เขียนโดย : </b><?php echo $row_news_003['news_post_by']; ?></p>
            <p> <b>วันเวลาที่เขียน : </b><?php echo date('d/m/Y H:i:s', strtotime($row_news_003['news_date'])); ?> </p>

            <a href="view_news.php?id=<?= $row_news_003['news_id']; ?>" class="btn btn-info btn-xs">
              <span class="glyphicon glyphicon-search"> </span> อ่านเพิ่มเติม </a>
            <hr style="border: 1px solid #d6d4d4;">
          </div>
        <?php
      }
      ?>
      </div>
      <!-- ข่าวสารแจ้งวันหยุด -->
      <div class="col-lg-6">
        <h3> ข่าวสารแจ้งวันหยุด
          <a class="btn btn-primary btn-xs" href="show_by_newstype.php?id=004">ดูทั้งหมด</a>
        </h3>
        <hr style="border: 1px solid #d6d4d4;">
        <?php
        while ($row_news_004 = mysqli_fetch_assoc($res_news_004)) {
          ?>
          <div class="col-md-4" align="center">
            <img src="news_image/<?php echo $row_news_004['news_filename']; ?>" style="width:80%;height:auto;">
          </div>
          <div class="col-md-8" style="width:60%;height:auto;"><br>
            <p><b> <?php echo $row_news_004['news_topic']; ?> </b></p> <!-- หัวข้อข่าว -->
            <p> <b>เขียนโดย : </b><?php echo $row_news_004['news_post_by']; ?></p>
            <p> <b>วันเวลาที่เขียน : </b><?php echo date('d/m/Y H:i:s', strtotime($row_news_004['news_date'])); ?> </p>

            <a href="view_news.php?id=<?= $row_news_004['news_id']; ?>" class="btn btn-info btn-xs">
              <span class="glyphicon glyphicon-search"> </span> อ่านเพิ่มเติม </a>
            <hr style="border: 1px solid #d6d4d4;">
          </div>
        <?php
      }
      ?>
      </div>
      <div class="col-xs-12"><br></div>
      <!-- ข่าวสารอื่นๆ -->
      <div class="col-xs-12">
        <h3> ข่าวสารอื่นๆ
          <a class="btn btn-primary btn-xs" href="show_by_newstype.php?id=005">ดูทั้งหมด</a>
        </h3>
        <hr style="border: 1px solid #d6d4d4;">
        <?php
        while ($row_news_005 = mysqli_fetch_assoc($res_news_005)) {
          ?>
          <div class="row">
            <div class="col-md-4" align="center">
              <img src="news_image/<?php echo $row_news_005['news_filename']; ?>" style="width:40%;height:auto;">
            </div>
            <div class="col-md-8"><br>
              <p> <b> <?php echo $row_news_005['news_topic']; ?> </b></p> <!-- หัวข้อข่าว -->
              <p> <b>เขียนโดย : </b><?php echo $row_news_005['news_post_by']; ?></p>
              <p> <b>วันเวลาที่เขียน : </b><?php echo date('d/m/Y H:i:s', strtotime($row_news_005['news_date'])); ?> </p>
              <br />
              <a href="view_news.php?id=<?= $row_news_005['news_id']; ?>" class="btn btn-info btn-xs">
                <span class="glyphicon glyphicon-search"> </span> อ่านเพิ่มเติม </a>
            </div>
          </div>
          <hr style="border: 1px solid #d6d4d4;">
        <?php
      }
      ?>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</div>

<?php include('footer.php');  ?>