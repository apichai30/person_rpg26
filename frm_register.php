<?php
session_start();
include('header.php');
// include('slide.php');
include('banner.php');
include('navbar.php');
?>
<!-- start body -->
<div class="container">
	<div class="row">
		<div class="col-md-6 col-lg-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading" style="text-align: center">
					<h3 class="panel-title">สมัครสมาชิก</h3>
				</div>
				<div class="panel-body table-responsive">
					<div style="text-align: center">
						<h4>ประวัติส่วนตัว</h4>
					</div>
					<form method="post" action="register.php" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
						<table width="100" border="10" class="table table-bordered">
							<tr>
								<td width="150" align="center">
									<h5>ชื่อผู้ใช้</h5>
								</td>
								<td colspan="2">
									<input class="form-control" id="username" required pattern="^[a-zA-Z0-9_.-][^\W_]*$" title="กรอกได้เฉพาะตัวอักษรภาษาอังกฤษและตัวเลข" minlength="5" autofocus name="username" placeholder="ชื่อผู้ใช้" type="text" autocomplete="off">
								</td>
							</tr>
							<tr>
								<td width="150" align="center">
									<h5>รหัสผ่าน</h5>
								</td>
								<td colspan="2">
									<input class="form-control" id="password" required pattern="^[a-zA-Z0-9_.-][^\W_]*$" title="กรอกได้เฉพาะตัวอักษรภาษาอังกฤษและตัวเลข" minlength="5" name="password" placeholder="รหัสผ่าน" type="password" autocomplete="off">
								</td>
							</tr>
							<tr>
								<td width="200" align="center">
									<h5>เลือกคำนำหน้า</h5>
								</td>
								<td>
									<label><input type="radio" name="profile_prefix" required title="กรุณาเลือกคำนำหน้า" value="1"> นาย</label>&nbsp;&nbsp;
									<label><input type="radio" name="profile_prefix" required title="กรุณาเลือกคำนำหน้า" value="2"> นาง</label>&nbsp;&nbsp;
									<label><input type="radio" name="profile_prefix" required title="กรุณาเลือกคำนำหน้า" value="3"> นางสาว</label>
								</td>

							</tr>
							<tr>
								<td width="200" align="center">
									<h5>เลือกเพศ</h5>
								</td>
								<td>
									<label><input type="radio" name="profile_gender" required title="กรุณาเลือกเพศ" value="1"> ชาย</label>&nbsp;&nbsp;
									<label><input type="radio" name="profile_gender" required title="กรุณาเลือกเพศ" value="2"> หญิง</label>
								</td>
							</tr>
							<tr>
							<tr>
								<td width="200" align="center">
									<h5>ชื่อ</h5>
								</td>
								<td>
									<input type="text" name="profile_fname" required pattern="[a-zA-Z0-9ก-๙]{5,100}" title="กรอกได้เฉพาะตัวอักษรและตัวเลข" minlength="4" class="form-control" id="profile_fname" placeholder="ชื่อ">
								</td>
							</tr>
							<tr>
								<td width="200" align="center">
									<h5>สกุล</h5>
								</td>
								<td>
									<input type="text" name="profile_lname" required pattern="[a-zA-Z0-9ก-๙]{5,100}" title="กรอกได้เฉพาะตัวอักษรและตัวเลข" minlength="4" class="form-control" id="profile_lname" placeholder="นามสกุล">
								</td>
							</tr>
							<tr>
								<td width="200" align="center">
									<h5>ปี/เดือน/วัน เกิด</h5>
								</td>
								<td>
									<input type="text" name="profile_birth" required title="กรุณาเลือกวันเกิด" class="form-control" id="profile_birth_date" placeholder="ตัวอย่าง ปี/เดือน/วัน" autocomplete="off">
								</td>
							</tr>
							<tr>
								<td width="200" align="center">
									<h5>ที่อยู่อาศัย</h5>
								</td>
								<td>
									<input type="text" name="profile_address" required title="กรอกที่อยู่ปัจจุบัน" minlength="4" class="form-control" id="profile_address" placeholder="ที่อยู่อาศัย">
									&nbsp;<small class="text-muted">ตัวอย่าง 100 หมู่ 1 ต.เมือง อ.เมือง จ.ลำปาง<small>
								</td>
							<tr>
								<td width="200" align="center">
									<h5>รหัสไปรษณีย์</h5>
								</td>
								<td>
									<input type="text" name="profile_zipcode" required pattern="[\d][^\W_]*" title="กรอกรหัสไปรษณีย์เป็นตัวเลขเท่านั่น" minlength="5" maxlength="5" class="form-control" id="profile_zipcode" placeholder="รหัสไปรษณีย์">
								</td>
							</tr>
							<tr>
								<td width="200" align="center">
									<h5>อีเมล์</h5>
								</td>
								<td>
									<input type="email" name="profile_email" required title="กรอกอีเมล์" class="form-control" id="profile_email" pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}*$" placeholder="Example@example.com">
								</td>
							</tr>
							<tr>
								<td width="200" align="center">
									<h5>เบอร์โทรศัพท์</h5>
								</td>
								<td>
									<input type="text" name="profile_phone" required pattern="[\d][^\W_]*" title="กรอกเบอร์โทรศัพท์เป็นตัวเลขเท่านั่น" class="form-control" id="profile_phone" placeholder="ตัวอย่าง เบอร์โทรศัพท์ 0888888888" maxlength="10" minlength="10">
								</td>
							</tr>
							<tr>
								<td width="200" align="center">
									<h5>เลือกภาพประจำตัว</h5>
								</td>
								<td>
									<input class="form-control-file" type="file" name="profile_image" id="profile_image" accept="image/gif,image/jpeg,image/jpg,image/png">
								</td>
							</tr>
				</div>
				</table>
				<div style="text-align: center">
					<h4>ข้อมูลราชการ</h4>
				</div>
				<table width="100" border="10" class="table table-bordered">
					<tr>
						<td width="200" align="center">
							<h5>ตำแหน่ง</h5>
						</td>
						<td>
							<select class="form-control" name="per_info_rank">
								<option value="0">กรุณาเลือกตำแหน่ง</option>
								<option value="1">ผู้อำนวยการโรงเรียน</option>
								<option value="2">รองผู้อำนวยการโรงเรียน</option>
								<option value="3">หัวหน้าฝ่ายงาน</option>
								<option value="4">ครูประจำวิชา</option>
								<option value="5">ครูพิเศษ</option>
								<option value="6">พนักงานทั่วไป</option>
								<option value="7">นักศึกษาฝึกประสบการณ์</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="200" align="center">
							<h5>วิชาสอน</h5>
						</td>
						<td>
							<select class="form-control" name="per_info_subjects">
								<option value="0">กรุณาเลือกวิชาสอน</option>
								<option value="1">ภาษาไทย</option>
								<option value="2">วิทยาศาสตร์</option>
								<option value="3">คณิตศาสตร์</option>
								<option value="4">สังคมศึกษา</option>
								<option value="5">ภาษาต่างประเทศ</option>
								<option value="6">การงานอาชีพและเทคโนโลยี</option>
								<option value="7">สุขศึกษาและพลศึกษา</option>
								<option value="8">ศิลปะ</option>
								<option value="0">ไม่มี</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width="200" align="center">
							<h5>สถานะ</h5>
						</td>
						<td>
							<select class="form-control" name="per_info_cate">
								<option value="0">กรุณาเลือกสถานะ</option>
								<option value="1">ข้าราชการครู</option>
								<option value="2">ข้าราชการทั่วไป</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="200" align="center">
							<h5>ฝ่ายงาน</h5>
						</td>
						<td>
							<select class="form-control" name="per_info_depar">
								<option value="0">กรุณาเลือกฝ่ายงาน</option>
								<option value="1">ผู้บริหารโรงเรียน</option>
								<option value="2">ฝ่ายวิชาการ</option>
								<option value="3">ฝ่ายงบประมาณ</option>
								<option value="4">ฝ่ายบุคคล</option>
								<option value="5">ฝ่ายทั่วไป</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="200" align="center">
							<h5>ระดับการศึกษาสูงสุด</h5>
						</td>
						<td>
							<select class="form-control" name="per_info_educa">
								<option value="0">กรุณาเลือกระดับการศึกษา</option>
								<option value="1">จบการศึกษาขั้นพื้นฐาน</option>
								<option value="2">จบมัธยมศึกษาปีที่ 6</option>
								<option value="3">จบประกาศนียบัตรวิชาชีพ</option>
								<option value="4">จบประกาศนียบัตรวิชาชีพชั้นสูง</option>
								<option value="5">จบปริญญาตรี</option>
								<option value="5">จบปริญญาโท</option>
								<option value="5">จบปริญญาเอก</option>
							</select>
						</td>
					</tr>
				</table>
				<div class="row">
					<div class="form-group">
						<div class="col-md-4">
							<td>
								<label for="newsfilename">
									<span id="showtext" style="color:red;">
										<!-- <h5>*ตัวอย่างภาพประจำตัว</h5> -->
									</span>
								</label>
							</td>
						</div>
						<div class="col-md-6">
							<br><img id="showimage" width="60%" height="auto">
						</div>
					</div>
				</div><br>
				<div class="form-group" align="center">
					<a href="index.php" class="btn btn-info" role="button">
						<span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
					</a>
					<button type="reset" class="btn btn-warning"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;ค่าเริ่มต้น</button>
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;สมัครสมาชิก</button>
				</div>
				</form>
				<div style="text-align: right"><br>
					<a href="index.php">
						<p>กลับหน้าหลัก</p>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="clear"></div>
</div>
<!-- end body -->


<?php
include('footer.php');
?>