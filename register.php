<?php
include('header.php');
require 'config_db/connectdb.php';
// echo '<pre>';
// print_r($_POST); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();

// tbl_login
$login_username = $_POST['username'];
$login_password = $_POST['password'];

// tbl_profile
$tbl_profile_prefix = $_POST['profile_prefix'];
$tbl_profile_fname = $_POST['profile_fname'];
$tbl_profile_lname = $_POST['profile_lname'];
$tbl_profile_gender = $_POST['profile_gender'];
$tbl_profile_birth = $_POST['profile_birth'];
$tbl_profile_address = $_POST['profile_address'];
$tbl_profile_zipcode = $_POST['profile_zipcode'];
$tbl_profile_email = $_POST['profile_email'];
$tbl_profile_phone = $_POST['profile_phone'];
// $tbl_profile_image = $_FILES['profile_image'];

// tbl_per_info
$tbl_per_info_rank = $_POST['per_info_rank'];
$tbl_per_info_subjects = $_POST['per_info_subjects'];
$tbl_per_info_cate = $_POST['per_info_cate'];
$tbl_per_info_depar = $_POST['per_info_depar'];
$tbl_per_info_educa = $_POST['per_info_educa'];
// echo '<pre>';
// print_r($_FILES); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// echo exit();

$check_user = "SELECT * FROM tbl_login  WHERE login_username = '$login_username'";
$res1 = mysqli_query($dbcon, $check_user) or die(mysqli_error($dbcon));
$num_user = mysqli_num_rows($res1);
if ($num_user > 0) {
    //ถ้ามี username นี้อยู่ในระบบแล้วให้แจ้งเตือน
    echo "<script>";
    echo "alert(' ชื่อผู้ใช้ นี้ถูกใช้งานแล้ว กรุณาสมัครใหม่อีกครั้ง');";
    echo "window.location='frm_register.php';";
    echo "</script>";
    echo exit();
} else {
    $check_email = "SELECT * FROM tbl_profile  WHERE tbl_profile_email = '$tbl_profile_email'";
    $res2 = mysqli_query($dbcon, $check_email) or die(mysqli_error($dbcon));
    $num_email = mysqli_num_rows($res2);
    if ($num_email > 0) {
        //ถ้ามี email นี้อยู่ในระบบแล้วให้แจ้งเตือน
        echo "<script>";
        echo "alert(' อีเมล์ นี้ถูกใช้งานแล้ว กรุณาสมัครใหม่อีกครั้ง');";
        echo "window.location='frm_register.php';";
        echo "</script>";
        echo exit();
    }
}
// upload image
// echo '<pre>';
// print_r($_FILES); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();
if ($_FILES['profile_image']['size'] > 0 && $_FILES['profile_image']['error'] == 0) {
    $image_ext = pathinfo(basename($_FILES['profile_image']['name']), PATHINFO_EXTENSION);
    $new_image_name = 'img_profile_' . uniqid() . "." . $image_ext;
    $image_path = "profile_image/";
    $image_upload_path = $image_path . $new_image_name;
    $success = move_uploaded_file($_FILES['profile_image']['tmp_name'], $image_upload_path);


    $sql = "INSERT INTO tbl_profile (tbl_profile_prefix, tbl_profile_fname, tbl_profile_lname,
                                            tbl_profile_gender, tbl_profile_birth, tbl_profile_address,
                                            tbl_profile_zipcode, tbl_profile_email, tbl_profile_phone,
                                            tbl_profile_image) 
                    VALUES ('$tbl_profile_prefix', '$tbl_profile_fname', '$tbl_profile_lname',
                            '$tbl_profile_gender', '$tbl_profile_birth', '$tbl_profile_address',
                            '$tbl_profile_zipcode', '$tbl_profile_email', '$tbl_profile_phone',
                            '$new_image_name')";
} else {
    $sql = "INSERT INTO tbl_profile (tbl_profile_prefix, tbl_profile_fname, tbl_profile_lname,
                                            tbl_profile_gender, tbl_profile_birth, tbl_profile_address,
                                            tbl_profile_zipcode, tbl_profile_email, tbl_profile_phone,
                                            tbl_profile_image) 
                    VALUES ('$tbl_profile_prefix', '$tbl_profile_fname', '$tbl_profile_lname',
                            '$tbl_profile_gender', '$tbl_profile_birth', '$tbl_profile_address',
                            '$tbl_profile_zipcode', '$tbl_profile_email', '$tbl_profile_phone',
                            'user_default.png')";
}
//เข้ารหัส password
$salt = 'tikde78uj4ujuhlaoikiksakeidke';
$hash_login_password = hash_hmac('sha256', $login_password, $salt);

$sql2 = "INSERT INTO tbl_login (tbl_profile_id, login_username, login_password) 
                    VALUES ((SELECT MAX(tbl_profile_id) FROM tbl_profile), '$login_username', '$hash_login_password')";
// VALUES (((SELECT MAX(tbl_profile_id) FROM tbl_profile)+1), '$login_username', '$hash_login_password')";

$sql3 = "INSERT INTO tbl_per_info (tbl_profile_id, tbl_per_info_rank, tbl_per_info_subjects,tbl_per_info_cate,
                    tbl_per_info_depar, tbl_per_info_educa) 
                VALUES ((SELECT MAX(tbl_profile_id) FROM tbl_profile),'$tbl_per_info_rank', '$tbl_per_info_subjects',
                '$tbl_per_info_cate' , '$tbl_per_info_depar' , '$tbl_per_info_educa' )";

$result = mysqli_query($dbcon, $sql);
$result2 = mysqli_query($dbcon, $sql2);
$result3 = mysqli_query($dbcon, $sql3);


if ($result && $result2 && $result3) {
    // header("Location: alert.php?code=1");
    echo '<script>';
    echo 'alert(" ลงทะเบียนสำเร็จแล้ว \n หมายเหตุ ท่านยังไม่สามารถเข้าใช้งานได้กรุณาติดต่อผู้ดูแลระบบ");';
    echo 'window.location="index.php";';
    echo '</script>';
} else {
    echo "เกิดข้อผิดพลาด ".mysqli_error($dbcon);
}

mysqli_close($dbcon);
