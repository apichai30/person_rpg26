<?php
session_start();
if (isset($_SESSION['is_member']) or isset($_SESSION['is_admin']) or isset($_SESSION['is_news'])) { } else {
  echo ("<script>
window.alert('กรุณาเข้าสู่ระบบก่อนทำรายการ');
window.location.href='index.php';
</script>");
}
include('header.php');
include('banner.php');
include('navbar.php');

require 'config_db/connectdb.php';

$per_portf_id = $_GET['id']; //ได้มาจาก tbl_profile_i
$fname = $_GET['per_name'];
// echo '<pre>';
// echo print_r($_GET);//เช็คค่า array ที่ส่งมา
// exit;
// echo '<pre>';

// เลือกข้อมูล
// $sql = "SELECT * FROM tbl_per_portf WHERE tbl_profile_id='$per_portf_id'";
$sql = "SELECT pro.tbl_profile_fname,pro.tbl_profile_lname,portf.*
FROM tbl_profile AS pro 
INNER JOIN tbl_per_portf AS portf ON pro.tbl_profile_id = portf.tbl_profile_id
WHERE pro.tbl_profile_id='$per_portf_id'";
$res_portf = mysqli_query($dbcon, $sql);

// $row_portf = mysqli_num_rows($res_portf);

?>

<div class="container">
  <div class="col-xs-12">
    <!-- --------------------------------------------------------------------------------------------------------------------- -->
    <!-- บอกตำแหน่งที่อยู่ -->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
        <li class="breadcrumb-item"><a href="account.php">บัญชีของฉัน</a></li>
        <li class="breadcrumb-item active" aria-current="page">ดูผลงาน</li>
      </ol>
    </nav>
    <!-- บอกตำแหน่งที่อยู่ -->
    <!-- ปุ่มเพิ่ม -->
    <h2>ผลงานของคุณ..<?php echo $fname ?>..ทั้งหมด</h2>
    <div class="btn-group" role="group" aria-label="Basic example">
      <button type="button" class="btn btn-success">
        <span class="glyphicon glyphicon-cog"></span>&nbsp;การจัดการ
      </button>
      <a href="frm_add_portf.php?id=<?= $per_portf_id; ?>&per_name=<?= $fname; ?>" class="btn btn-success" role="button">
        <span class="glyphicon glyphicon-plus"></span>&nbsp;เพิ่มผลงาน
      </a>
    </div>
    <!-- ปุ่มเพิ่ม -->
    <table id="table_news" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%"><br><br>
      <thead>
        <tr class="bg-info" align="center">
          <th>รหัส</th>
          <th>ไฟล์แนบ</th>
          <th>รายละเอียด</th>
          <th>วันและเวลา</th>
          <th></th>
        </tr>
      </thead>
      <!-- --------------------------------------------------------------------------------------------------------------------- -->
      <?php
      while ($row_portf = mysqli_fetch_assoc($res_portf)) {
        // while ($row_portf) {
        ?>
        <tr>
          <th> <?php echo $row_portf['portf_id']; ?> </th>
          <th><a href="profile_file/<?php echo $row_portf['portf_name']; ?>" target="_blank"><?php echo $row_portf['portf_name']; ?></a></th>
          <th> <?php echo $row_portf['portf_detail']; ?> </th>
          <th> <?php echo date('d/m/Y H:i:s', strtotime($row_portf['portf_date'])); ?> </th>
          <th style="text-align: center">
            <a href="del_portf.php?id=<?= $row_portf['portf_id'] ?>&pro_id=<?= $row_portf['tbl_profile_id'] ?>&per_name=<?= $fname; ?>">
              <button type="submit" class="btn btn-danger" onclick="return confirm('คุณต้องการที่จะลบข้อมูล\nกดปุ่ม OK เพื่อยืนยัน')" style="width:120px">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;ลบ
              </button>
            </a>
          </th>
        </tr>
      <?php
    }
    ?>
    </table>
    <div class="form-group" align="center">
      <a href="account.php" class="btn btn-info" role="button">
        <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
      </a>
    </div>
  </div>
  <div class="clear"></div>
</div>

<?php include('footer.php');  ?>