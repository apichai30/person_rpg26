<div class="container-fluid">
  <div class="col-md-12">
    <div class="row">
      <nav class="navbar navbar-default" data-spy="affix" data-offset-top="200">

        <!-- ส่วนแสดงเมนูตอนที่ย่อเล็กลง -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">ระบบข้อมูลบุคลากรและผลงาน</a>
          <!-- <a class="navbar-brand" style="margin-left: 10rem;"></a> -->
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <!-- <li><a href="../index.php">หน้าหลัก</a></li> -->
            <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;หน้าแรก</a></li> <!-- ไปหน้า main ใน login_system -->
            <li class="dropdown">
              <a href="index_per.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ข้อมูลบุคลากร<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="index_per.php#depar_1">ฝ่ายบริหารงานโรงเรียน</a></li>
                <li><a href="index_per.php#depar_2">ฝ่ายบริหารงานวิชาการ</a></li>
                <li><a href="index_per.php#depar_3">ฝ่ายบริหารงานงบประมาณ</a></li>
                <li><a href="index_per.php#depar_4">ฝ่ายบริหารงานบุคคล</a></li>
                <li><a href="index_per.php#depar_5">ฝ่ายบริหารงานทั่วไป</a></li>
                <li role="separatorc" class="divider"></li>
                <li><a href="index_per.php">ดูทั้งหมด</a></li>
              </ul>
            </li> <!-- ไปหน้า ....... ใน login_system -->
            <li><a href="index_news.php">ข่าวสารประชาสัมพันธ์</a></li> <!-- ไปหน้า index_news ใน login_system -->
            <li><a href="index_web.php">กระดานถาม-ตอบ</a></li> <!-- ไปหน้า ....... ใน ....... -->
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <?php
            if (isset($_SESSION['is_admin']) or isset($_SESSION['is_member']) or isset($_SESSION['is_news'])) {
              ?>
              <li>
                <a href="#">คุณ <?php echo $_SESSION["tbl_profile_fname"] . " " . $_SESSION['tbl_profile_lname'];?></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>&nbsp;ตัวเลือก<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="login_system/main.php"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span>&nbsp;สำหรับผู้ดูแล</a></li>
                  <li><a href="account.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;บัญชีของฉัน</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="logout.php"><span class="glyphicon glyphicon-off" aria-hidden="true"></span>&nbsp;ออกจากระบบ</a></li>
                </ul>
              <?php
            } else {
              ?>
              <li><a href="login_system/index.php">เข้าสู่ระบบ</a></li>
              <li><a href="frm_register.php">สมัครสมาชิก</a></li>
            <?php
          }
          ?>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</div>