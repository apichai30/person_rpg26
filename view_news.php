<?php
session_start();
include('header.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';

$news_id = $_GET['id'];
$sql = "SELECT * FROM tbl_news WHERE news_id='$news_id'";
$res_news = mysqli_query($dbcon, $sql);
$row_news = mysqli_fetch_array($res_news);
//echo print_r($row_user); //เช็คค่า array ที่ส่งมา
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <!-- บอกตำแหน่งที่อยู่ -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_news.php">ข่าวสารประชาสัมพันธ์</a></li>
                    <li class="breadcrumb-item active" aria-current="page">ดูข่าว</li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="col-md-8">
                <h2><?php echo $row_news['news_topic']; ?></h2><!-- หัวข้อข่าว -->
                <p>เขียนโดย : <b><?php echo $row_news['news_post_by']; ?></b>
                    วันที่โพสต์ : <?php echo date('d/m/Y H:i:s', strtotime($row_news['news_date'])); ?>
                    ประเภท : <b><?php
                                if ($row_news['news_status'] == 0) {
                                    echo 'ข่าวทั่วไป';
                                } elseif ($row_news['news_status'] == 1) {
                                    echo 'ข่าวเด่น';
                                } else {
                                    #?????????
                                } ?></b>
                </p>
            </div>
            <div class="col-xs-12" style="text-align: center;margin-top: 5rem;">
                <img src="news_image/<?php echo $row_news['news_filename']; ?>" width="40%" height="auto">
            </div>
            <div class="col-xs-12" style="text-align: center; margin-bottom: 20rem;"><br>
                <?php echo $row_news['news_detail']; ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php include('footer.php');  ?>