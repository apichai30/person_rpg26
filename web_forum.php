<?php
session_start();
if (isset($_SESSION['is_member']) or isset($_SESSION['is_admin']) or isset($_SESSION['is_news'])) { } else {
    echo ("<script>
        window.alert('กรุณาเข้าสู่ระบบก่อนทำรายการ');
        window.location.href='login_system/index.php';
        </script>");
}
include('header.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';

$boardtype_id = $_GET['id'];

if ($boardtype_id == 001) {
    $sql = "SELECT *FROM tbl_board WHERE boardtype_id=001 ORDER BY board_id DESC";
    $res_forum = mysqli_query($dbcon, $sql);
} elseif ($boardtype_id == 002) {
    $sql = "SELECT *FROM tbl_board WHERE boardtype_id=002 ORDER BY board_id DESC";
    $res_forum = mysqli_query($dbcon, $sql);
} elseif ($boardtype_id == 003) {
    $sql = "SELECT *FROM tbl_board WHERE boardtype_id=003 ORDER BY board_id DESC";
    $res_forum = mysqli_query($dbcon, $sql);
} else {
    $sql = "SELECT *FROM tbl_board WHERE boardtype_id=004 ORDER BY board_id DESC";
    $res_forum = mysqli_query($dbcon, $sql);
}
// echo print_r($sql);
// exit;
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_web.php">กระดาน ถาม-ตอบ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">คำถาม
                        <?php
                        if ($boardtype_id == 001) {
                            echo 'เกี่ยวกับเรื่องโรงเรียน';
                        } elseif ($boardtype_id == 002) {
                            echo 'เกี่ยวกับเรื่องวิชาการ';
                        } elseif ($boardtype_id == 003) {
                            echo 'เกี่ยวกับเรื่องกิจกรรม';
                        } else {
                            echo 'เกี่ยวกับเรื่องทั่วไป';
                        }
                        ?>
                    </li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <h3> กระดาน ถาม-ตอบ
                <?php
                if ($boardtype_id == 001) {
                    echo 'เกี่ยวกับเรื่องโรงเรียน';
                } elseif ($boardtype_id == 002) {
                    echo 'เกี่ยวกับเรื่องวิชาการ';
                } elseif ($boardtype_id == 003) {
                    echo 'เกี่ยวกับเรื่องกิจกรรม';
                } else {
                    echo 'เกี่ยวกับเรื่องทั่วไป';
                }
                ?> ทั้งหมด</h3>
            <!-- ปุ่มเพิ่ม -->
            <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-success">
                    <span class="glyphicon glyphicon-cog"></span>&nbsp;การจัดการ
                </button>
                <a href="frm_add_board.php?id=<?php echo $boardtype_id ?>" class="btn btn-success" role="button">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;เพิ่มหัวข้อ
                </a>
            </div>
            <!-- /ปุ่มเพิ่ม -->
            <div class="container-fluid">
                <div class="row"><br>
                    <div class="panel-group">
                        <div class="panel panel-info">
                            <div class="panel-heading" style="color:black;">
                                <h4>พูดคุย
                                    <?php
                                    if ($boardtype_id == 001) {
                                        echo 'เกี่ยวกับเรื่องโรงเรียน';
                                    } elseif ($boardtype_id == 002) {
                                        echo 'เกี่ยวกับเรื่องวิชาการ';
                                    } elseif ($boardtype_id == 003) {
                                        echo 'เกี่ยวกับเรื่องกิจกรรม';
                                    } else {
                                        echo 'เกี่ยวกับเรื่องทั่วไป';
                                    }
                                    ?>
                                </h4>
                            </div>
                            <?php
                            while ($row_forum = mysqli_fetch_assoc($res_forum)) {
                                ?>
                                <div class="panel-body">
                                    <!-- <?php echo $row_forum['board_topic']; ?>
                                                                            เขียนโดย : <?php echo $row_forum['board_post_by']; ?> -->
                                    <table class="table-striped">
                                        <tbody>
                                            <td width="600px" height="50px">
                                                <a href="view_topic.php?id=<?= $row_forum['board_id']; ?>&board_type=<?= $boardtype_id ?> " style="color:black;">
                                                    <?php echo $row_forum['board_topic']; ?>
                                                </a>
                                            </td>
                                            <td width="200px"> เขียนโดย : <?php echo $row_forum['board_post_by']; ?></td>
                                            <td width="200px"> <?php echo date('d/m/Y H:i:s', strtotime($row_forum['board_date'])); ?></td>
                                            <?php
                                            if ($_SESSION['tbl_profile_fname'] ==  $row_forum['board_post_by']) {
                                                ?>
                                                <td width="100px">
                                                    <a class="btn btn-warning btn-xs" href="frm_edit_board.php?board_id=<?= $row_forum['board_id'] ?>&board_type=<?= $boardtype_id ?>">
                                                        <span class="glyphicon glyphicon-wrench"></span>
                                                    </a>
                                                    <a class="btn btn-danger btn-xs" href="del_board.php?id=<?= $row_forum['board_id']; ?>&board_type=<?= $boardtype_id ?>" onclick="return confirm('คุณต้องการที่จะลบข้อมูล\nกดปุ่ม OK เพื่อยืนยัน')">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </a>
                                                </td>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php
                        }
                        ?>
                        </div><!-- /div class="panel panel-info" -->
                    </div><!-- /div class="panel-group" -->
                </div><!-- /div class="row" -->
            </div><br><br>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php include('footer.php');  ?>