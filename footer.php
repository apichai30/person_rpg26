<!-- go to top -->
<div id="toTop">ขึ้นบน</div>

<!-- COPY RIGHT  Version.2 -->
<div class="footer" style="width:100%; margin-left:0rem; margin-top:0; padding: 2rem; text-align:center; background:#2196f3;">
    <p style="color: #fff; font-size: medium;">&copy; สงวนลิขสิทธิ์ สาขาวิชาวิศวกรรมซอฟต์แวร์ มหาวิทยาลัยราชภัฎลำปาง</p>
</div>

<!-- bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script src="js/dataTables.responsive.min.js"></script>
<script src="js/responsive.bootstrap.min.js"></script>
<!-- datepicker -->
<script src="js/bootstrap-datepicker.min.js"></script>
<!-- ckediter -->
<script src="ckeditor/ckeditor.js"></script>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.3"></script>

<!-- CKEDITOR -->
<script type="text/javascript">
    CKEDITOR.replace('news_detail', {
        language: 'th'
    });
</script>

<!-- datepicker -->
<script type="text/javascript">
    // $(function() {
    //     $(function() {
    //         $('#profile_birth_date').datepicker({
    //             format: 'yyyy/mm/dd',
    //             maxDate: '+1m +10d',
    //             minDate: -10
    //         });
    //     });
    // });
    $(document).ready(function() {
        // Date Object
        $('#profile_birth_date').datepicker({
            dateFormat: "yyyy-mm-dd",
            minDate: "-60y"
        });
    });
</script>

<!-- แสดงตัวอย่างรูปภาพ -->
<script>
    var filename = document.getElementById('profile_image');
    filename.onchange = function() {
        var files = filename.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(files);
        reader.onload = function() {
            var result = reader.result;
            document.getElementById('showimage').src = result;
            document.getElementById("showtext").innerHTML = '*ตัวอย่างภาพประจำตัว (ใหม่)';
        };
    };
</script>
<!-- เช็คค่าว่างเมือ่กดปุ่มบันทึกไฟล์ -->
<script type="text/javascript">
    function upfile() {
        if (document.getElementById('file_portf').value == "") {
            alert('กรุณาเลือกไฟล์');
            return false;
        }
    }
</script>
<!-- go to top -->
<script>
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });

    $("#toTop").click(function() {
        $("body, html").animate({
            scrollTop: 0
        }, 500);
    });
</script>
</body>

</html>