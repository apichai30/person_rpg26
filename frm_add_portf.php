<?php
session_start();
if (isset($_SESSION['is_member']) or isset($_SESSION['is_admin']) or isset($_SESSION['is_news'])) { } else {
    echo ("<script>
  window.alert('กรุณาเข้าสู่ระบบก่อนทำรายการ');
  window.location.href='index.php';
  </script>");
}
include('header.php');
include('banner.php');
include('navbar.php');
$per_portf_id = $_GET['id']; //ได้มาจาก tbl_profile_id
$fname = $_GET['per_name'];
?>
<!-- start body -->
<div class="container">
    <div class="row">
        <!-- บอกตำแหน่งที่อยู่ -->
        <div class="col-md-6 col-lg-8 col-md-offset-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="account.php">บัญชีของฉัน</a></li>
                    <li class="breadcrumb-item"><a href="view_portf.php?id=<?= $per_portf_id; ?>&per_name=<?= $fname; ?>">ดูผลงาน</a></li>
                    <li class="breadcrumb-item active" aria-current="page">เพิ่มผลงาน</li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center">
                    <h3 class="panel-title">เพิ่มผลงาน</h3>
                </div>
                <div class="panel-body table-responsive">
                    <form method="post" action="add_portf.php?id=<?= $per_portf_id; ?>&per_name=<?= $fname; ?>" accept-charset="UTF-8" role="form" enctype="multipart/form-data" onSubmit="JavaScript:return upfile();">
                        <table width="100" border="10" class="table table-bordered">
                            <tr>
                            <td width="250" align="center">
                                    <h5>คำอธิบาย</h5>
                                </td>
                                <td colspan="2"><input type="text" name="portf_detail" required class="form-control" placeholder="คำอธิบาย"></td>
                            </tr>
                            <tr>
                            <td width="250" align="center">
                                    <h5>เลือกไฟล์</h5>
                                </td>
                                <td colspan="2"><input type="file" name="file_portf[]" id="file_portf" class="form-control-file" accept=".doc, .docx,.ppt, .pptx,.txt,.pdf" multiple="multiple"></td>
                            </tr>
                        </table><br>
                        <div class="form-group" align="center">
                            <a href="view_portf.php?id=<?= $per_portf_id; ?>&per_name=<?= $fname; ?>" class="btn btn-info" role="button">
                                <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                            </a>
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!-- end body -->


<?php
include('footer.php');
?>