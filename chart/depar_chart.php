<?php
require '../config_db/connectdb.php';
//เรียกข้อมูลจากตาราง tbl_profile 
$sql = "SELECT tbl_per_info_depar ,COUNT(*)
            AS number FROM tbl_per_info GROUP BY tbl_per_info_depar";
$res_depar = mysqli_query($dbcon, $sql);

// echo '<pre>';
// print_r($sql); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();
?>
<html>

<head>
    <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['ฝ่ายบริหารงาน', 'จำนวน'],
                <?php
                while ($row_depar = mysqli_fetch_array($res_depar)) {
                    if ($row_depar["tbl_per_info_depar"] == 1) {
                        $depar = "ฝ่ายบริหารงานโรงเรียน";
                    } elseif ($row_depar["tbl_per_info_depar"] == 2) {
                        $depar = "ฝ่ายบริหารงานิชาการ";
                    } elseif ($row_depar["tbl_per_info_depar"] == 3) {
                        $depar = "ฝ่ายบริหารงานงบประมาณ";
                    } elseif ($row_depar["tbl_per_info_depar"] == 4) {
                        $depar = "ฝ่ายบริหารงานบุคคล";
                    } elseif ($row_depar["tbl_per_info_depar"] == 5) {
                        $depar = "ฝ่ายบริหารงาน่วไป";
                    } else {
                        $depar = "ไม่ทราบข้อมูล";
                    }
                    echo "['" . $depar . "'," . $row_depar["number"] . "],";
                }
                ?>
            ]);
            var options = {
                title: 'แบ่งตามฝ่ายบริหารงาน',
                backgroundColor: 'transparent',
                fontSize: 14,
            };

            var chart = new google.visualization.PieChart(document.getElementById('depar_piechart'));
            chart.draw(data, options);
        }
    </script>
</head>