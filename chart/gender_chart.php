<?php
require '../config_db/connectdb.php';
//เรียกข้อมูลจากตาราง tbl_profile 
$sql = "SELECT tbl_profile_gender ,COUNT(*)
            AS number FROM tbl_profile GROUP BY tbl_profile_gender";
$res_gender = mysqli_query($dbcon, $sql);

// echo '<pre>';
// print_r($sql); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();
?>
<html>

<head>
    <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['เพศ', 'จำนวน'],
                <?php
                while ($row_gender = mysqli_fetch_array($res_gender)) {
                    if ($row_gender["tbl_profile_gender"] == 1) {
                        $gender = "ชาย";
                    } else {
                        $gender = "หญิง";
                    }
                    echo "['" . $gender . "'," . $row_gender["number"] . "],";
                }
                ?>
            ]);
            var options = {
                title: 'แบ่งตามเพศ',
                backgroundColor: 'transparent',
                fontSize: 14,
            };
            var chart = new google.visualization.PieChart(document.getElementById('gender_piechart'));
            chart.draw(data, options);
        }
    </script>
</head>