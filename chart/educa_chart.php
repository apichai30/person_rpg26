<?php
require '../config_db/connectdb.php';
//เรียกข้อมูลจากตาราง tbl_profile 
$sql = "SELECT tbl_per_info_educa ,COUNT(*)
            AS number FROM tbl_per_info GROUP BY tbl_per_info_educa";
$res_educa = mysqli_query($dbcon, $sql);

// echo '<pre>';
// print_r($sql); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();
?>
<html>

<head>
    <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['วุฒิการศึกษา', 'จำนวน'],
                <?php
                while ($row_educa = mysqli_fetch_array($res_educa)) {

                    if ($row_educa["tbl_per_info_educa"] == 1) {
                        $educa = "จบการศึกษาขั้นพื้นฐาน";
                    } elseif ($row_educa["tbl_per_info_educa"] == 2) {
                        $educa = "จบมัธยมศึกษาชั้นปีที่ 6";
                    } elseif ($row_educa["tbl_per_info_educa"] == 3) {
                        $educa = "จบประกาศนียบัตรวิชาชีพ";
                    } elseif ($row_educa["tbl_per_info_educa"] == 4) {
                        $educa = "จบประกาศนียบัตรวิชาชีพชั้นสูง";
                    } elseif ($row_educa["tbl_per_info_educa"] == 5) {
                        $educa = "จบปริญญาตรี";
                    } elseif ($row_educa["tbl_per_info_educa"] == 6) {
                        $educa = "จบปริญญาโท";
                    } elseif ($row_educa["tbl_per_info_educa"] == 7) {
                        $educa = "จบปริญญาเอก";
                    } else {
                        $educa = "ไม่ทราบข้อมูล";
                    }
                    echo "['" . $educa . "'," . $row_educa["number"] . "],";
                }
                ?>
            ]);
            var options = {
                title: 'แบ่งตามฝ่ายวุฒิการศึกษา',
                backgroundColor: 'transparent',
                fontSize: 14,
                // pieHole: 0.2,
            };
            var chart = new google.visualization.PieChart(document.getElementById('educa_piechart'));
            chart.draw(data, options);
        }
    </script>
</head>