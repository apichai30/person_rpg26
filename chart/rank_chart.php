<?php
require '../config_db/connectdb.php';
//เรียกข้อมูลจากตาราง tbl_profile 
$sql = "SELECT tbl_per_info_rank ,COUNT(*)
            AS number FROM tbl_per_info GROUP BY tbl_per_info_rank";
$res_rank = mysqli_query($dbcon, $sql);

// echo '<pre>';
// print_r($sql); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();
?>
<html>

<head>
    <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['ตำแหน่งงาน', 'จำนวน'],
                <?php
                while ($row_rank = mysqli_fetch_array($res_rank)) {
                    if ($row_rank["tbl_per_info_rank"] == 1) {
                        $rank = "ผู้อำนวนการโรงเรียน";
                    } elseif ($row_rank["tbl_per_info_rank"] == 2) {
                        $rank = "รองผู้อำนวนการโรงเรียน";
                    } elseif ($row_rank["tbl_per_info_rank"] == 3) {
                        $rank = "หัวหน้าฝ่ายบริหารงาน";
                    } elseif ($row_rank["tbl_per_info_rank"] == 4) {
                        $rank = "ครูประจำวิชา";
                    } elseif ($row_rank["tbl_per_info_rank"] == 5) {
                        $rank = "ครูพิเศษ";
                    } elseif ($row_rank["tbl_per_info_rank"] == 6) {
                        $rank = "พนักงานทั่วไป";
                    } elseif ($row_rank["tbl_per_info_rank"] == 7) {
                        $rank = "นักศึกษาฝึกงาน";
                    } else {
                        $rank = "ไม่ทราบข้อมูล";
                    }
                    echo "['" . $rank . "'," . $row_rank["number"] . "],";
                }
                ?>
            ]);
            var options = {
                title: 'แบ่งตามตำแหน่ง',
                backgroundColor: 'transparent',
                fontSize: 14,
            };
            var chart = new google.visualization.PieChart(document.getElementById('rank_piechart'));
            chart.draw(data, options);
        }
    </script>
</head>