<?php
session_start();
// if (isset($_SESSION['is_admin'])) {
//   header("Location: index.php");
// }
include('header.php');
include('banner.php');
// include('slide.php');
include('navbar.php');
// echo print_r($row_user); //เช็คค่า array ที่ส่งมา


// echo isset($A) ? $A : '';
// ECHO $A;
require 'config_db/connectdb.php';
$newstype_id = $_GET['id'];

if ($newstype_id == 001) {
    $sql = "SELECT * FROM tbl_news WHERE newstype_id=001 ORDER BY news_id DESC";
    $res_news = mysqli_query($dbcon, $sql);
} elseif ($newstype_id == 002) {
    $sql = "SELECT *FROM tbl_news WHERE newstype_id=002 ORDER BY news_id DESC";
    $res_news = mysqli_query($dbcon, $sql);
} elseif ($newstype_id == 003) {
    $sql = "SELECT *FROM tbl_news WHERE newstype_id=003 ORDER BY news_id DESC";
    $res_news = mysqli_query($dbcon, $sql);
} elseif ($newstype_id == 004) {
    $sql = "SELECT *FROM tbl_news WHERE newstype_id=004 ORDER BY news_id DESC";
    $res_news = mysqli_query($dbcon, $sql);
} elseif ($newstype_id == 005) {
    $sql = "SELECT *FROM tbl_news WHERE newstype_id=005 ORDER BY news_id DESC";
    $res_news = mysqli_query($dbcon, $sql);
} else {
    $sql = "SELECT *FROM tbl_news WHERE news_status=1 ORDER BY news_id DESC";
    $res_news = mysqli_query($dbcon, $sql);
}

// echo print_r($res_news);
// exit;
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <!-- บอกตำแหน่งที่อยู่ -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_news.php">ข่าวสารประชาสัมพันธ์</a></li>
                    <li class="breadcrumb-item active" aria-current="page">ประเภท</li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <h3>
                <?php
                if ($newstype_id == 001) {
                    echo 'ข่าวสารที่น่าสนใจ';
                } elseif ($newstype_id == 002) {
                    echo 'ข่าวสารแจ้งการประชุม';
                } elseif ($newstype_id == 003) {
                    echo 'ข่าวสารแจ้งกิจกรรม';
                } elseif ($newstype_id == 004) {
                    echo 'ข่าวสารแจ้งวันหยุด';
                } elseif ($newstype_id == 005) {
                    echo 'ข่าวสารอื่นๆ';
                }else{
                    echo 'ข่าวเด่น';
                }
                ?> ทั้งหมด
            </h3>
            <?php
            if (isset($_SESSION['is_news']) or isset($_SESSION['is_admin'])) {  //ต้องสร้างตัวแปรมาใหม่เอาไว้แสดงปุ่มเพิ่ม ผู้ที่จะเห็นได้คือ ฝ่ายบุคลากร กับแอดมิน
                if ($newstype_id != 10) { // ค่า newstype_id ต้องไม่เท่ากับ 10 จะแสดงปุ่มเพิ่ม
                    ?>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-success">
                            <span class="glyphicon glyphicon-cog"></span>&nbsp;การจัดการ
                        </button>
                        <a href="frm_add_news.php?id=<?php echo $newstype_id; ?>" class="btn btn-success" role="button">
                            <span class="glyphicon glyphicon-plus"></span>&nbsp;เพิ่มข่าว
                        </a>
                    </div>
                <?php
            }
        }
        ?>
            <div class="col-xs-12"><br>
                <?php
                while ($row_news_status = mysqli_fetch_assoc($res_news)) {
                    ?>
                    <div class="row">
                        <div class="col-md-4" align="center">
                            <img src="news_image/<?php echo $row_news_status['news_filename']; ?>" width="40%" height="auto">
                        </div>
                        <?php
                        // if (isset($_SESSION['is_news']) or isset($_SESSION['is_admin'])) {  //ต้องสร้างตัวแปรมาใหม่เอาไว้แสดงปุ่มเพิ่ม ผู้ที่จะเห็นได้คือ ฝ่ายบุคลากร กับแอดมิน
                        // if ($_SESSION['tbl_profile_fname'] ==  $row_news_status['news_post_by']) {

                        // เช็คว่ามีตัวแปรไหม ถ้ามีให้ไปทำ if ต่อ
                        if (isset($_SESSION['tbl_profile_fname']) && $_SESSION['tbl_profile_fname'] ==  $row_news_status['news_post_by']) {

                            ?>
                            <div align="right">
                                <a class="btn btn-warning btn-xs" href="frm_edit_news.php?id=<?= $row_news_status['news_id']; ?>&type_id=<?= $row_news_status['newstype_id']; ?>">
                                    <span class="glyphicon glyphicon-wrench">แก้ไข</span>
                                </a>
                                <a class="btn btn-danger btn-xs" href="del_news.php?id=<?= $row_news_status['news_id']; ?>&type_id=<?= $row_news_status['newstype_id']; ?>" onclick="return confirm('คุณต้องการที่จะลบข้อมูล\nกดปุ่ม OK เพื่อยืนยัน')">
                                    <span class="glyphicon glyphicon-remove">ลบ</span>
                                </a>
                            </div>
                        <?php
                    }  //ต้องสร้างตัวแปรมาใหม่เอาไว้แสดงปุ่มเพิ่ม ผู้ที่จะเห็นได้คือ ฝ่ายข่าว กับแอดมิน
                    ?>
                        <div class="col-md-8">
                            <b> <?php echo $row_news_status['news_topic']; ?> </b><br> <!-- หัวข้อข่าว -->
                            <p> เขียนโดย : <b><?php echo $row_news_status['news_post_by']; ?></b> วันที่เขียน : <?php echo date('d/m/Y H:i:s', strtotime($row_news_status['news_date'])); ?></p>
                            <br />
                            <a href="view_news.php?id=<?= $row_news_status['news_id']; ?>" class="btn btn-info btn-xs">
                                <span class="glyphicon glyphicon-search"> </span> อ่านเพิ่มเติม </a>
                        </div>
                    </div>
                    <hr style="border: 1px solid #d6d4d4;">
                <?php
            }
            ?>
            </div>
            <div align="center">
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php include('footer.php');  ?>