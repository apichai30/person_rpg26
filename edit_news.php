<?php
include('config_db/connectdb.php');

$news_id = $_POST['news_id'];
$newstype_id = $_POST['news_type'];
$news_topic = $_POST['news_topic'];
$news_detail = $_POST['news_detail'];
$news_filename = $_POST['news_filename'];
$news_status = $_POST['news_status'];
$news_post_by = $_POST['update_by_name'];

if (is_uploaded_file($_FILES['news_filename']['tmp_name'])) {
    //delete old image
    $sql_select = "SELECT news_filename FROM tbl_news WHERE news_id='$news_id' ";
    $result_image = mysqli_query($dbcon, $sql_select);
    $row_image = mysqli_fetch_assoc($result_image);
    $image_old = $row_image['news_filename'];
    unlink("news_image/".$image_old);


    //upload new image
    $image_ext = pathinfo(basename($_FILES['news_filename']['name']), PATHINFO_EXTENSION);
    $new_image_name = 'img_news_'.uniqid().".".$image_ext;
    $image_path = "news_image/";
    $image_upload_path = $image_path.$new_image_name;
    $success = move_uploaded_file($_FILES['news_filename']['tmp_name'],$image_upload_path);
    
    $sql_image = "UPDATE tbl_news SET news_filename='$new_image_name' WHERE news_id='$news_id'";
    mysqli_query($dbcon, $sql_image);
    
    if ($success==false) {
            echo "ไม่สามารถอัพโหลดรูปภาพได้";
            exit();
        }
}




//edit data
$sql = "UPDATE tbl_news SET news_topic='$news_topic', news_detail='$news_detail', news_status='$news_status', news_date=NOW(), newstype_id='$newstype_id', news_post_by='$news_post_by' 
        WHERE (news_id='$news_id')";
$result = mysqli_query($dbcon, $sql);
    if ($result) {
        // echo "บันทึกข้อมูลเรียบร้อย";
        header("Location: show_by_newstype.php?id= $newstype_id");
    } else {
        echo "เกิดข้อผิดพลาด ".mysqli_error($dbcon);
    }



?>