<?php
session_start();
include('header.php');
// include('slide.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';
// ดึงข้อมูลจาก tbl_news ที่ news_status
$news_status = "SELECT * FROM tbl_news WHERE news_status=1 ORDER BY news_id DESC LIMIT 5";
$res_news_status = mysqli_query($dbcon, $news_status);

// ดึงข้อมูลจาก tbl_board ที่ boardtype_id
$row_boardtype_001 = "SELECT * FROM tbl_board WHERE boardtype_id='001' ORDER BY board_id DESC LIMIT 5";
$res_board_001 = mysqli_query($dbcon, $row_boardtype_001);

$row_boardtype_002 = "SELECT * FROM tbl_board WHERE boardtype_id='002' ORDER BY board_id DESC LIMIT 5";
$res_board_002 = mysqli_query($dbcon, $row_boardtype_002);

$row_boardtype_003 = "SELECT * FROM tbl_board WHERE boardtype_id='003' ORDER BY board_id DESC LIMIT 5";
$res_board_003 = mysqli_query($dbcon, $row_boardtype_003);

$row_boardtype_004 = "SELECT * FROM tbl_board WHERE boardtype_id='004' ORDER BY board_id DESC LIMIT 5";
$res_board_004 = mysqli_query($dbcon, $row_boardtype_004);

//รับข้อมูลผ.อ ร.ร
$sql1 = "SELECT tbl_per_info.tbl_per_info_rank ,tbl_profile.tbl_profile_fname ,tbl_profile.tbl_profile_id, tbl_profile.tbl_profile_lname ,tbl_profile.tbl_profile_image
        FROM tbl_per_info INNER JOIN tbl_profile 
        ON tbl_per_info.tbl_profile_id = tbl_profile.tbl_profile_id
        WHERE tbl_per_info.tbl_per_info_rank=1";
$res_profile1 = mysqli_query($dbcon, $sql1);
?>
<div class="container-fluid" style="padding-right: 30px;padding-left: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-5">
                <h3>ข่าวประชาสัมพันธ์</h3>
                <hr style="border: 1px solid #d6d4d4;">
                <div class="row">
                    <?php
                    while ($row_news_status = mysqli_fetch_assoc($res_news_status)) {
                        ?>
                        <div class="col-lg-4" align="center">
                            <img src="news_image/<?php echo $row_news_status['news_filename']; ?>" style="width:40%;height:auto;">
                        </div>
                        <div class="col-lg-8">
                            <a href="view_news.php?id=<?= $row_news_status['news_id']; ?>" style="text-decoration:none; color:black; font-size:2rem">
                                <p><?php echo $row_news_status['news_topic']; ?></p> <!-- หัวข้อข่าว -->
                            </a>
                            <p> <b>เขียนโดย : </b><?php echo $row_news_status['news_post_by']; ?></p>
                            <p> <b>วันเวลาที่เขียน : </b><?php echo date('d/m/Y H:i:s', strtotime($row_news_status['news_date'])); ?> </p>
                            <br>
                            <!-- <span class="glyphicon glyphicon-search"> </span> อ่านเพิ่มเติม -->
                            <hr style="border: 1px solid #d6d4d4;">

                        </div>
                    <?php } ?>
                </div><br>
                <div class="row">
                    <div align="center">
                        <a href="index_news.php" class="btn btn-info" style="width:50%; padding:5px; margin-bottom: 10rem;">
                            <span class="glyphicon glyphicon-search"></span> ดูทั้งหมด
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <h3>กระดานถาม-ตอบ</h3>
                <hr style="border: 1px solid #d6d4d4;">
                <div class="row">
                    <div class="panel-group" style="padding-left: 2rem; padding-right: 2rem;">
                        <div class="panel panel-info">
                            <div class="panel-heading" style="color:black;">พูดคุยเกี่ยวกับเรื่องโรงเรียน&nbsp;&nbsp;
                                <!-- <a class="btn btn-primary btn-xs" href="web_forum.php?id=001">ดูทั้งหมด</a> -->
                            </div>
                            <?php
                            while ($row_001 = mysqli_fetch_assoc($res_board_001)) {
                                ?>
                                <div class="panel-body">
                                    <!-- <?php echo $row_001['board_topic']; ?>
                                                                                เขียนโดย : <?php echo $row_001['board_post_by']; ?> -->
                                    <table>
                                        <tbody>
                                            <td width="300px">
                                                <a href="view_topic.php?id=<?= $row_001['board_id']; ?>&board_type=<?= $row_001['boardtype_id']; ?> " style="color:black;">
                                                    <?php echo $row_001['board_topic']; ?>
                                                </a>
                                            </td>
                                            <td width="250px"> เขียนโดย : <?php echo $row_001['board_post_by']; ?></td>
                                            <td width="150px"> <?php echo date('d/m/Y H:i:s', strtotime($row_001['board_date'])); ?></td>
                                        </tbody>
                                    </table>
                                </div>
                            <?php
                        }
                        ?>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading" style="color:black;">พูดคุยเกี่ยวกับเรื่องวิชาการ&nbsp;&nbsp;
                                <!-- <a class="btn btn-primary btn-xs" href="web_forum.php?id=002">ดูทั้งหมด</a> -->
                            </div>
                            <?php
                            while ($row_002 = mysqli_fetch_assoc($res_board_002)) {
                                ?>
                                <div class="panel-body">
                                    <table>
                                        <tbody>
                                            <td width="300px">
                                                <a href="view_topic.php?id=<?= $row_002['board_id']; ?>&board_type=<?= $row_002['boardtype_id']; ?> " style="color:black;">
                                                    <?php echo $row_002['board_topic']; ?>
                                                </a>
                                            </td>
                                            <td width="250px"> เขียนโดย : <?php echo $row_002['board_post_by']; ?></td>
                                            <td width="150px"> <?php echo date('d/m/Y H:i:s', strtotime($row_002['board_date'])); ?></td>
                                        </tbody>
                                    </table>
                                </div>
                            <?php
                        }
                        ?>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading" style="color:black;">พูดคุยเกี่ยวกับเรื่องกิจกรรม&nbsp;&nbsp;
                                <!-- <a class="btn btn-primary btn-xs" href="web_forum.php?id=003">ดูทั้งหมด</a> -->
                            </div>
                            <?php
                            while ($row_003 = mysqli_fetch_assoc($res_board_003)) {
                                ?>
                                <div class="panel-body">
                                    <table>
                                        <tbody>
                                            <td width="300px">
                                                <a href="view_topic.php?id=<?= $row_003['board_id']; ?>&board_type=<?= $row_003['boardtype_id']; ?> " style="color:black;">
                                                    <?php echo $row_003['board_topic']; ?>
                                                </a>
                                            </td>
                                            <td width="250px"> เขียนโดย : <?php echo $row_003['board_post_by']; ?></td>
                                            <td width="150px"> <?php echo date('d/m/Y H:i:s', strtotime($row_003['board_date'])); ?></td>
                                        </tbody>
                                    </table>
                                </div>
                            <?php
                        }
                        ?>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading" style="color:black;">พูดคุยเกี่ยวกับเรื่องทั่วไป&nbsp;&nbsp;
                                <!-- <a class="btn btn-primary btn-xs" href="web_forum.php?id=004">ดูทั้งหมด</a> -->
                            </div>
                            <?php
                            while ($row_004 = mysqli_fetch_assoc($res_board_004)) {
                                ?>
                                <div class="panel-body">
                                    <table>
                                        <tbody>
                                            <td width="300px">
                                                <a href="view_topic.php?id=<?= $row_004['board_id']; ?>&board_type=<?= $row_004['boardtype_id']; ?> " style="color:black;">
                                                    <?php echo $row_004['board_topic']; ?>
                                                </a>
                                            </td>
                                            <td width="250px"> เขียนโดย : <?php echo $row_004['board_post_by']; ?></td>
                                            <td width="150px"> <?php echo date('d/m/Y H:i:s', strtotime($row_004['board_date'])); ?></td>
                                        </tbody>
                                    </table>
                                </div>
                            <?php
                        }
                        ?>
                        </div><br><br>
                        <div class="row">
                            <div align="center">
                                <a href="index_web.php" class="btn btn-info" style="width:50%; padding:5px; margin-bottom: 10rem;">
                                    <span class="glyphicon glyphicon-search"></span> ดูทั้งหมด
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--  -->
            <div class="col-lg-2" style="/*background-color: #fff*/;margin-top:1rem" align="center">
                <h4 id="rank_1">ผู้อำนวยการโรงเรียน</h4>
                <!-- แสดงรายการ -->
                <?php while ($row_profile1 = mysqli_fetch_assoc($res_profile1)) { ?>
                    <div class="row">
                        <div class="card" align="center">
                            <img src="profile_image/<?php echo $row_profile1['tbl_profile_image']; ?>" width="40%"><br>
                            <p class="card-text"><?php echo $row_profile1['tbl_profile_fname'] . " " . $row_profile1['tbl_profile_lname']; ?></p>
                            <a href="view_per.php?per_id=<?= $row_profile1['tbl_profile_id']; ?>" class="btn btn-info">ดูเพิ่มเติม</a>
                        </div><br><br>
                    </div>
                <?php } ?>
                <div class="row">
                    <div align="center">
                        <div class="fb-page" data-href="https://www.facebook.com/rpk26/" data-tabs="timeline,events,messages " data-width="200px" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" style="padding-left:0.5rem; padding-right: 0.5rem;">
                            <blockquote cite="https://www.facebook.com/rpk26/" class="fb-xfbml-parse-ignore">
                                <a href="https://www.facebook.com/rpk26/">โรงเรียนราชประชานุเคราะห์ 26</a>
                            </blockquote>
                        </div>
                    </div><br><br>
                </div>
            </div><!-- close -->
        </div>
    </div>
    <div class="clear"></div>
</div>

<?php
include('footer.php');
?>