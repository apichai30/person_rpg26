<?php
session_start();
require 'config_db/connectdb.php';
include('header.php');
// include('slide.php');s
include('banner.php');
include('navbar.php');

$search = $_GET['search'];

// tbl_per_info.tbl_per_info_depar ,tbl_profile.tbl_profile_fname ,tbl_profile.tbl_profile_id, tbl_profile.tbl_profile_lname ,tbl_profile.tbl_profile_image
$sql = "SELECT tbl_per_info.* ,tbl_profile.*
        FROM tbl_per_info INNER JOIN tbl_profile 
        ON tbl_per_info.tbl_profile_id = tbl_profile.tbl_profile_id
        WHERE tbl_profile.tbl_profile_fname 
        LIKE '%$search%'
        OR tbl_profile.tbl_profile_lname
        LIKE '%$search%'
        ORDER BY tbl_profile.tbl_profile_id ASC";
$res_search = mysqli_query($dbcon, $sql);
// echo print_r($sql); //เช็คค่า array ที่ส่งมา

?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <!-- บอกตำแหน่งที่อยู่ -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item active" aria-current="page">ข้อมูลบุคลกร</li>
                </ol>
            </nav>
            <form method="get" action="">
                <div align="right">
                    <div class="form-group" style="width:300px;">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="กรอกชื่อหรือนามสกุล" name="search" required>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-success">ค้นหาข้อมูล</button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="col-lg-12">
                <hr style="border: 1px solid #d6d4d4;">
                <h3 id="depar_1">แสดงข้อมูลที่ค้นหา</h3><br>
            </div>
            <!-- แสดงรายการ -->
            <?php while ($row_search = mysqli_fetch_assoc($res_search)) { ?>
                <div class="col-md-4">
                    <div class="card" align="center">
                        <img src="profile_image/<?php echo $row_search['tbl_profile_image']; ?>" width="60%"><br><br>
                        <p class="card-text"><?php echo $row_search['tbl_profile_fname'] . " " . $row_search['tbl_profile_lname']; ?></p>
                        <a href="view_per.php?per_id=<?= $row_search['tbl_profile_id']; ?>" class="btn btn-info">ดูเพิ่มเติม</a>
                    </div><br><br>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php
include('footer.php');
?>