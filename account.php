<?php
session_start();
if (isset($_SESSION['is_member']) or isset($_SESSION['is_admin']) or isset($_SESSION['is_news'])) { } else {
    echo ("<script>
  window.alert('กรุณาเข้าสู่ระบบก่อนทำรายการ');
  window.location.href='index.php';
  </script>");
}
include('header.php');
include('banner.php');
// include('slide.php');
include('navbar.php');
include('config_db/connectdb.php');

$log_id = $_SESSION['login_id'];
$per_id = $_SESSION['profile_id'];

$sql = "SELECT pro.*,info.*,login.*
FROM tbl_profile AS pro
INNER JOIN tbl_per_info AS info ON info.tbl_profile_id=pro.tbl_profile_id
INNER JOIN tbl_login AS login ON login.tbl_profile_id=pro.tbl_profile_id
WHERE pro.tbl_profile_id='$per_id'";
// echo '<pre>';
// print_r($sql); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();

$res_per = mysqli_query($dbcon, $sql);
$row_per = mysqli_fetch_array($res_per);


?>
<!-- start body -->
<div class="container">
    <div class="row">
        <!-- <div class="col-md-12 col-md-offset-2">  col-md-offset-2 คือการเว้นช่วงไป 2 คอลัม -->
        <!-- บอกตำแหน่งที่อยู่ -->
        <div class="col-md-6 col-lg-8 col-md-offset-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item active" aria-current="page">บัญชีของฉัน</a></li>

                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center">
                    <h3 class="panel-title">บัญชีของฉัน</h3>
                </div>
                <div class="panel-body table-responsive">
                    <div style="text-align: center">
                        <h4>ประวัติส่วนตัว</h4>
                    </div>
                    <form method="post" action="edit_account.php" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
                        <div class="form-group" align="center">
                            <div>
                                <img src="profile_image/<?php echo $row_per['tbl_profile_image']; ?>" width="40%" height="auto">
                                <br>
                            </div>
                        </div>
                        <table width="100" border="10" class="table table-bordered">
                            <tr>
                                <td width="150" align="center">
                                    <h5>ชื่อผู้ใช้</h5>
                                </td>
                                <td colspan="2">
                                    <input class="form-control" id="username" required autofocus name="username" readonly placeholder="ชื่อผู้ใช้" type="text" value="<?php echo $row_per['login_username']; ?>">
                                    <small class="text-muted">หากท่านต้องการเปลี่ยนแปลงกรุณาติดต่อผู้ดูแลโดยตรง</small>
                                </td>
                            </tr>
                            <tr>
                                <td width="150" align="center">
                                    <h5>รหัสผ่าน</h5>
                                </td>
                                <td colspan="2">
                                    <input class="form-control" id="password" required name="password" readonly placeholder="รหัสผ่านเดิม" type="password" value="<?php echo $row_per['login_password']; ?>">
                                    <small class="text-muted">หากท่านต้องการเปลี่ยนแปลงกรุณาติดต่อผู้ดูแลโดยตรง</small>

                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>เลือกคำนำหน้า</h5>
                                </td>
                                <td>
                                    <!-- <label> -->
                                    <?php
                                    if ($row_per['tbl_profile_prefix'] == 1) {
                                        echo  '<input type="radio" value="1" name="profile_prefix" checked> ชาย &nbsp';
                                        echo  '&nbsp<input type="radio" value="2" name="profile_prefix"> นาง ';
                                        echo  '&nbsp<input type="radio" value="3" name="profile_prefix"> นางสาว <br><br>';
                                        // echo exit();
                                    } elseif ($row_per['tbl_profile_prefix'] == 2) {
                                        echo  '<input type="radio" value="1" name="profile_prefix"> นาย &nbsp';
                                        echo  '&nbsp<input type="radio" value="2" name="profile_prefix" checked> นาง ';
                                        echo  '&nbsp<input type="radio" value="3" name="profile_prefix"> นางสาว <br><br>';
                                        // echo print_r($row_per['tbl_profile_prefix']);
                                        // echo exit();
                                    } else {
                                        echo  '<input type="radio" value="1" name="profile_prefix"> นาย &nbsp';
                                        echo  '&nbsp<input type="radio" value="2" name="profile_prefix"> นาง ';
                                        echo  '&nbsp<input type="radio" value="3" name="profile_prefix" checked> นางสาว <br><br>';
                                        // echo exit;
                                    }
                                    // echo print_r($row_per['tbl_profile_prefix']);
                                    // echo exit();
                                    ?>
                                    <!-- </label> -->
                                    <!-- <label><input type="radio" name="profile_prefix" required value="1"> นาย</label>&nbsp;&nbsp; -->
                                    <!-- <label><input type="radio" name="profile_prefix" required value="2"> นาง</label>&nbsp;&nbsp; -->
                                    <!-- <label><input type="radio" name="profile_prefix" required value="3"> นางสาว</label> -->
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>เลือกเพศ</h5>
                                </td>
                                <td>
                                    <!-- <label> -->
                                    <?php
                                    if ($row_per['tbl_profile_gender'] == 1) {
                                        echo  '<input type="radio" value="1" name="profile_gender" checked> ชาย &nbsp';
                                        echo  '&nbsp<input type="radio" value="2" name="profile_gender"> หญิง <br><br>';
                                        // echo print_r(['tbl_profile_gender']);
                                    } else {
                                        echo  '<input type="radio" value="1" name="profile_gender"> ชาย &nbsp';
                                        echo  '&nbsp<input type="radio" value="2" name="profile_gender" checked> หญิง <br><br>';
                                        // echo print_r(['tbl_profile_gender']);
                                    }
                                    ?>
                                    <!-- </label> -->
                                    <!-- <label><input type="radio" name="profile_gender" required value="1"> ชาย</label>&nbsp;&nbsp; -->
                                    <!-- <label><input type="radio" name="profile_gender" required value="2"> หญิง</label> -->
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>ชื่อ</h5>
                                </td>
                                <td><input type="text" name="profile_fname" required pattern="^[a-zA-Z0-9ก-๙][^\W_]*$" title="กรอกได้เฉพาะตัวอักษร" minlength="4" class="form-control" id="profile_fname" placeholder="ชื่อ" value="<?php echo $row_per['tbl_profile_fname']; ?>"></td>
                                <!-- regular ใช้กำหนด pattern="[ก-๙]{3,100}" title="กรอกได้เฉพาะตัวอักษรภาษาไทย" -->
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>สกุล</h5>
                                </td>
                                <!-- regular ใช้กำหนด pattern="[ก-๙]{3,100}" title="กรอกได้เฉพาะตัวอักษรภาษาไทย" -->
                                <td><input type="text" name="profile_lname" required pattern="^[a-zA-Z0-9ก-๙][^\W_]*$" title="กรอกได้เฉพาะตัวอักษร" minlength="4" class="form-control" id="profile_lname" placeholder="นามสกุล" value="<?php echo $row_per['tbl_profile_lname']; ?>"></td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>ปี/เดือน/วัน เกิด</h5>
                                </td>
                                <td colspan="2"><input type="text" name="profile_birth" required class="form-control" id="profile_birth_date" value="<?php echo date('Y/m/d', strtotime($row_per['tbl_profile_birth'])); ?>" autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>ที่อยู่อาศัย</h5>
                                </td>
                                <td colspan="2">
                                    <input type="text" name="profile_address" required class="form-control" id="tbl_profile_address" placeholder="ที่อยู่อาศัย" value="<?php echo $row_per['tbl_profile_address']; ?>">
                                    <small class="text-muted">ตัวอย่าง 100 หมู่ 1 ต.เมือง อ.เมือง จ.ลำปาง<small>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>รหัสไปรษณีย์</h5>
                                </td>
                                <td colspan="2"><input type="text" name="profile_zipcode" required pattern="[0-9][^\W_]*" title="กรอกรหัสไปรษณีย์เป็นตัวเลขเท่านั่น" minlength="5" maxlength="5" class="form-control" id="tbl_profile_zipcode" placeholder="รหัสไปรษณีย์" maxlength="5" value="<?php echo $row_per['tbl_profile_zipcode']; ?>"></td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>อีเมล์</h5>
                                </td>
                                <td colspan="2"><input type="email" name="profile_email" required pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}*$" class="form-control" id="tbl_profile_email" placeholder="Example@example.com" value="<?php echo $row_per['tbl_profile_email']; ?>"></td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>เบอร์โทรศัพท์</h5>
                                </td>
                                <td colspan="2"><input type="text" name="profile_phone" required pattern="[\d][^\W_]*" title="กรอกเบอร์โทรศัพท์เป็นตัวเลขเท่านั่น"  class="form-control" id="tbl_profile_phone" placeholder="ตัวอย่าง เบอร์โทรศัพท์ 0888888888" maxlength="10" minlength="10" value="<?php echo $row_per['tbl_profile_phone']; ?>"></td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>เลือกภาพประจำตัว</h5>
                                </td>
                                <td><input class="form-control-file" type="file" name="profile_image" id="profile_image" accept="image/gif,image/jpeg,image/jpg,image/png"></td>
                                </td>
                            </tr>
                        </table>
                        <div style="text-align: center">
                            <h4>ข้อมูลราชการ</h4>
                        </div>
                        <table width="100" border="10" class="table table-bordered">
                            <tr>
                                <td width="200" align="center">
                                    <h5>ตำแหน่ง</h5>
                                </td>
                                <td>
                                    <?php
                                    if ($row_per['tbl_per_info_rank'] == 1) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_rank" value="ผู้อำนวยการโรงเรียน" readonly>';
                                    } elseif ($row_per['tbl_per_info_rank'] == 2) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_rank" value="รองผู้อำนวยการโรงเรียน" readonly>';
                                    } elseif ($row_per['tbl_per_info_rank'] == 3) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_rank" value="หัวหน้าฝ่ายงาน" readonly>';
                                    } elseif ($row_per['tbl_per_info_rank'] == 4) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_rank" value="ครูประจำวิชา" readonly>';
                                    } elseif ($row_per['tbl_per_info_rank'] == 5) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_rank" value="ครูพิเศษ" readonly>';
                                    } elseif ($row_per['tbl_per_info_rank'] == 6) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_rank" value="พนักงานทั่วไป" readonly>';
                                    } elseif ($row_per['tbl_per_info_rank'] == 7) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_rank" value="นักศึกษาฝึกประสบการณ์" readonly>';
                                    } else {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_rank" value="ไม่ทราบข้อมูล" readonly>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>วิชาสอน</h5>
                                </td>
                                <td>
                                    <?php
                                    if ($row_per['tbl_per_info_subjects'] == 1) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="ภาษาไทย" readonly>';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 2) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="วิทยาศาสตร์" readonly>';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 3) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="คณิตศาสตร์" readonly>';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 4) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="สังคมศึกษา" readonly>';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 5) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="ภาษาต่างประเทศ" readonly>';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 6) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="การงานอาชีพและเทคโนโลยี" readonly>';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 7) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="สุขศึกษาและพลศึกษา" readonly>';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 8) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="ศิลปะ" readonly>';
                                    } else {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_subjects" value="ไม่ทราบข้อมูล" readonly>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>สถานะ</h5>
                                </td>
                                <td>
                                    <?php
                                    if ($row_per['tbl_per_info_cate'] == 1) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_cate" value="ข้าราชการครู" readonly>';
                                    } elseif ($row_per['tbl_per_info_cate'] == 2) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_cate" value="ข้าราชการทั่วไป" readonly>';
                                    } else {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_cate" value="ไม่ทราบข้อมูล" readonly>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>ฝ่ายงาน</h5>
                                </td>
                                <td>
                                    <?php
                                    if ($row_per['tbl_per_info_depar'] == 1) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_depar" value="บริหารงานโรงเรียน" readonly>';
                                    } elseif ($row_per['tbl_per_info_depar'] == 2) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_depar" value="บริหารงานวิชาการ" readonly>';
                                    } elseif ($row_per['tbl_per_info_depar'] == 3) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_depar" value="บริหารงานงบประมาณ" readonly>';
                                    } elseif ($row_per['tbl_per_info_depar'] == 4) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_depar" value="บริหารงานบุคคล" readonly>';
                                    } elseif ($row_per['tbl_per_info_depar'] == 5) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_depar" value="บริหารงานทั่วไป" readonly>';
                                    } else {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_depar" value="ไม่ทราบข้อมูล" readonly>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>การศึกษาสูงสุด</h5>
                                </td>
                                <td>
                                    <?php
                                    if ($row_per['tbl_per_info_educa'] == 1) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_educa" value="จบการศึกษาขั้นพื้นฐาน" readonly>';
                                    } elseif ($row_per['tbl_per_info_educa'] == 2) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_educa" value="จบมัธยมศึกษาชั้นปีที่ 6" readonly>';
                                    } elseif ($row_per['tbl_per_info_educa'] == 3) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_educa" value="จบประกาศนียบัตรวิชาชีพ" readonly>';
                                    } elseif ($row_per['tbl_per_info_educa'] == 4) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_educa" value="จบประกาศนียบัตรวิชาชีพชั้นสูง" readonly>';
                                    } elseif ($row_per['tbl_per_info_educa'] == 5) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_educa" value="จบปริญญาตรี" readonly>';
                                    } elseif ($row_per['tbl_per_info_educa'] == 6) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_educa" value="จบปริญญาโท" readonly>';
                                    } elseif ($row_per['tbl_per_info_educa'] == 7) {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_educa" value="จบปริญญาเอก" readonly>';
                                    } else {
                                        echo  '<input type="text" class="form-control" name="tbl_per_info_educa" value="ไม่ทราบข้อมูล" readonly>';
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <div style="text-align: center">
                            <h4>ผลงานของฉัน</h4>
                        </div>
                        <table width="100" border="10" class="table table-bordered">
                            <tr>
                                <td width="200" align="center">
                                    <h5>ดูผลงาน</h5>
                                </td>
                                <td>
                                    <a href="view_portf.php?id=<?= $row_per['tbl_profile_id'] ?>&per_name=<?= $row_per['tbl_profile_fname']; ?>" class="btn btn-primary" role="button">
                                        <span class="glyphicon glyphicon-eye-open"></span>&nbsp;ดูผลงานของฉัน
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <!-- รูปประจำตัวใหม่ -->
                                    <td>
                                        <label>
                                            <span id="showtext" style="color:red; text-align:center">
                                            </span>
                                        </label>
                                    </td>
                                </div>
                                <div class="col-md-6">
                                    <img id="showimage" width="60%" height="auto">
                                </div>
                            </div>
                        </div><br>
                        <div align="center">
                            <input type="hidden" name="per_id" value="<?php echo $row_per['tbl_profile_id']; ?>">
                            <a href="index_per.php" class="btn btn-info" role="button">
                                <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                            </a>
                            <button type="reset" class="btn btn-warning"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;ค่าเริ่มต้น</button>
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;บันทึก
                            </button>
                        </div><br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!-- end body -->


<?php
include('footer.php');
?>