<?php
session_start();
if (isset($_SESSION['is_member']) or isset($_SESSION['is_admin']) or isset($_SESSION['is_news'])) { } else {
    echo ("<script>
        window.alert('กรุณาเข้าสู่ระบบก่อนทำรายการ');
        window.location.href='login_system/index.php';
        </script>");
}
include('header.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';
$board_id = $_GET['id'];
// echo print_r($_GET);
// exit;
?>
<!-- start body -->
<div class="container">
    <div class="row">
        <!-- บอกตำแหน่งที่อยู่ -->
        <div class="col-xs-10 col-xs-offset-1">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="main.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_web.php">กระดาน ถาม-ตอบ</a></li>
                    <li class="breadcrumb-item">
                        <?php
                        if ($board_id == 001) {
                            echo '<a href="web_forum.php?id=001">คำถาม เกี่ยวกับเรื่องโรงเรียน</a>';
                        } elseif ($board_id == 002) {
                            echo '<a href="web_forum.php?id=002">คำถาม เกี่ยวกับเรื่องวิชาการ</a>';
                        } elseif ($board_id == 003) {
                            echo '<a href="web_forum.php?id=003">คำถาม เกี่ยวกับเรื่องกิจกรรม</a>';
                        } else {
                            echo '<a href="web_forum.php?id=004">คำถาม เกี่ยวกับเรื่องทั่วไป</a>';
                        }
                        ?>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">เพิ่มหัวข้อ</li>
                    </li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center">
                    <h3 class="panel-title">เพิ่มหัวข้อ</h3>
                </div>
                <div class="panel-body">
                    <form id="form1" method="post" action="add_board.php" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
                        <table width="100" border="10" class="table table-bordered">
                            <tr>
                                <td for="boardtype" width="250" align="center">
                                    <h5>ประเภท</h5>
                                </td>
                                <td colspan="2">
                                    <?php
                                    if ($board_id == 001) {
                                        echo '<input type="text" name="boardtype_id" class="form-control" placeholder="เกี่ยวกับเรื่องโรงเรียน" readonly>
                                    <input type="hidden" name="boardtype_id" class="form-control" value="001">';
                                    } elseif ($board_id == 002) {
                                        echo '<input type="text" name="boardtype_id" class="form-control" placeholder="เกี่ยวกับเรื่องวิชาการ" readonly>
                                    <input type="hidden" name="boardtype_id" class="form-control" value="002">';
                                    } elseif ($board_id == 003) {
                                        echo '<input type="text" name="boardtype_id" class="form-control" placeholder="เกี่ยวกับเรื่องกิจกรรม" readonly>
                                    <input type="hidden" name="boardtype_id" class="form-control" value="003">';
                                    } else {
                                        echo '<input type="text" name="boardtype_id" class="form-control" placeholder="เกี่ยวกับเรื่องทั่วไป" readonly>
                                    <input type="hidden" name="boardtype_id" class="form-control" value="004">';
                                    }
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <td for="newstopic" width="250" align="center">
                                    <h5>หัวข้อ</h5>
                                </td>
                                <td colspan="2"><input type="text" name="board_topic" required class="form-control" placeholder="กรอกหัวข้อ"></td>
                            </tr>
                            <!-- -------------------------------------------------------------------------------------------------------- -->
                            <tr>
                                <td for="newsdetail" width="250" align="center">
                                    <h5>เนื้อหา</h5>
                                </td>
                                <td>
                                    <textarea type="text" name="news_detail" id="board_detail" required class="form-control" rows="10" cols="80" placeholder="รายละเอียด"></textarea>
                                </td>
                            </tr>
                        </table>
                        <tr>
                            <div align="center">
                                <?php
                                if ($board_id == 001) {
                                    echo '<a href="web_forum.php?id=001" class="btn btn-info" role="button">
                                            <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                        </a>';
                                    // header("Location: web_forum.php?id=001");
                                } elseif ($board_id == 002) {
                                    echo '<a href="web_forum.php?id=002" class="btn btn-info" role="button">
                                            <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                        </a>';
                                    // header("Location: web_forum.php?id=002");
                                } elseif ($board_id == 003) {
                                    echo '<a href="web_forum.php?id=003" class="btn btn-info" role="button">
                                            <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                        </a>';
                                    // header("Location: web_forum.php?id=003");
                                } else {
                                    echo '<a href="web_forum.php?id=004" class="btn btn-info" role="button">
                                            <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                        </a>';
                                    // header("Location: web_forum.php?id=004");
                                }
                                ?>
                                <!-- <a href="index_web.php" class="btn btn-info" role="button">
                                <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                            </a> -->
                                <input type="hidden" name="post_by_name" value="<?php echo $_SESSION["tbl_profile_fname"]; ?>">
                                <button type="reset" class="btn btn-warning"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;ค่าเริ่มต้น</button>
                                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;เพิ่มหัวข้อ</button>
                            </div>
                        </tr>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!-- end body -->


<?php
include('footer.php');
?>