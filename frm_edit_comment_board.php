<?php
session_start();
include('header.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';

$boardtype_id = $_GET['boardtype_id'];
$view_board_id = $_GET['board_id'];
$view_board_comment_id = $_GET['board_comment_id'];

$sql = "SELECT *FROM tbl_board
        WHERE board_id='$view_board_id'";

$res_view_board = mysqli_query($dbcon, $sql);
$row_view_board = mysqli_fetch_array($res_view_board);


$sql2 = "SELECT *FROM tbl_board_comment
        WHERE board_comment_id='$view_board_comment_id'";

$res_view_comment_board = mysqli_query($dbcon, $sql2);
$row_view_comment_board = mysqli_fetch_array($res_view_comment_board);

// echo '<pre>';
// print_r($_GET); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();

?>
<div class="container">
    <div class="col-xs-12 col-xs-offset-1">
        <div class="col-sm-9 col-lg-10">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="main.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_web.php">กระดาน ถาม-ตอบ</a></li>
                    <li class="breadcrumb-item">
                        <?php
                        if ($boardtype_id == 001) {
                            echo '<a href="web_forum.php?id=001">คำถาม เกี่ยวกับเรื่องโรงเรียน</a>';
                        } elseif ($boardtype_id == 002) {
                            echo '<a href="web_forum.php?id=002">คำถาม เกี่ยวกับเรื่องวิชาการ</a>';
                        } elseif ($boardtype_id == 003) {
                            echo '<a href="web_forum.php?id=003">คำถาม เกี่ยวกับเรื่องกิจกรรม</a>';
                        } else {
                            echo '<a href="web_forum.php?id=004">คำถาม เกี่ยวกับเรื่องทั่วไป</a>';
                        }
                        ?>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">แก้ไขความคิดเห็น</li>
                </ol>
            </nav>

            <!-- แสดงกระทู้ -->
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center">
                    <h3 class="panel-title">แก้ไขความคิดเห็น</h3>
                </div>
                <div class="panel-body">
                    <div class="">
                        <strong>หัวข้อเรื่อง <?php echo $row_view_board['board_topic']; ?></strong>
                        <p> โพสต์โดย <?php echo $row_view_board['board_post_by']; ?>
                            วันที่โพสต์ <?php echo date('d-m-Y H:i:s', strtotime($row_view_board['board_date'])); ?>
                        </p>
                        <strong> รายละเอียด </strong>
                        <p><?php echo $row_view_board['board_detail']; ?> </p>
                    </div>
                </div>
            </div>
            <!-- แสดงกระทู้ -->

            <!-- เขียนความคิดเห็น -->
            <div>
                <form id="form1" method="post" action="edit_comment_board.php" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
                    <h4>แก้ไขความคิดเห็น</h4>
                    <textarea type="text" name="news_detail" id="board_comment_detail" class="form-control" required rows="5" cols="50" placeholder="รายละเอียด">
                <?php echo $row_view_comment_board["board_comment_detail"]; ?>
                </textarea><br>
                    <div align="center">
                        <a href="view_topic.php?id=<?= $row_view_board['board_id']; ?>&board_type=<?= $row_view_board['boardtype_id']; ?>" class="btn btn-info" role="button">
                            <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                        </a>
                        <input type="hidden" name="id_board" value="<?php echo $row_view_board['board_id']; ?>">
                        <input type="hidden" name="boardtype_id" value="<?php echo $row_view_board['boardtype_id']; ?>">
                        <input type="hidden" name="board_comment_id" value="<?php echo $row_view_comment_board["board_comment_id"]; ?>">
                        <input type="hidden" name="comment_by_name" value="<?php echo $_SESSION["tbl_profile_fname"]; ?>">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;แก้ไขความคิดเห็น
                        </button>
                    </div>
                </form>
            </div><br>
            <!-- เขียนความคิดเห็น -->
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php include('footer.php');  ?>