<?php
session_start();
if (isset($_SESSION['is_member']) or isset($_SESSION['is_admin']) or isset($_SESSION['is_news'])) { } else {
    echo ("<script>
        window.alert('กรุณาเข้าสู่ระบบก่อนทำรายการ');
        window.location.href='login_system/index.php';
        </script>");
}
include('header.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';

$view_board_id = $_GET['id'];
$boardtype_id = $_GET['board_type'];
$sql = "SELECT *FROM tbl_board 
        INNER JOIN tbl_boardtype 
        ON tbl_board.boardtype_id = tbl_boardtype.boardtype_id 
        WHERE tbl_board.board_id='$view_board_id'";
$res_view_board = mysqli_query($dbcon, $sql);
$row_view_board = mysqli_fetch_array($res_view_board);
// print_r($_SESSION);die;

// $sql = "SELECT *FROM tbl_board_comment 
//         INNER JOIN tbl_board 
//         ON tbl_board_comment.board_id = tbl_board.board_id 
//         INNER JOIN tbl_boardtype
//         ON tbl_board_comment.boardtype_id = tbl_boardtype.boardtype_id 
//         WHERE tbl_board_comment.board_comment_id='$view_board_id'";

$sql2 = "SELECT *FROM tbl_board_comment 
        INNER JOIN tbl_board 
        ON tbl_board_comment.board_id = tbl_board.board_id 
        INNER JOIN tbl_boardtype
        ON tbl_board_comment.boardtype_id = tbl_boardtype.boardtype_id 
        WHERE tbl_board_comment.board_id='$view_board_id'
        ORDER BY tbl_board_comment.board_comment_id='$view_board_id'
        DESC";

$res_view_comment_board = mysqli_query($dbcon, $sql2);
// $row_view_comment_board = mysqli_fetch_array($res_view_comment_board);

// echo print_r($row_view_board); //เช็คค่า array ที่ส่งมา
// exit;

?>
<div class="container-fluid">
    <div class="row">
        <!-- <div class="col-lg-12 col-md-offset-2">  col-md-offset-2 คือการเว้นช่วงไป 2 คอลัม -->
        <!-- บอกตำแหน่งที่อยู่ -->
        <div class="col-xs-10 col-xs-offset-1">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_web.php">กระดาน ถาม-ตอบ</a></li>
                    <li class="breadcrumb-item">
                        <?php
                        if ($boardtype_id == 001) {
                            echo '<a href="web_forum.php?id=001">คำถาม เกี่ยวกับเรื่องโรงเรียน</a>';
                        } elseif ($boardtype_id == 002) {
                            echo '<a href="web_forum.php?id=002">คำถาม เกี่ยวกับเรื่องวิชาการ</a>';
                        } elseif ($boardtype_id == 003) {
                            echo '<a href="web_forum.php?id=003">คำถาม เกี่ยวกับเรื่องกิจกรรม</a>';
                        } else {
                            echo '<a href="web_forum.php?id=004">คำถาม เกี่ยวกับเรื่องทั่วไป</a>';
                        }
                        ?>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">อ่านหัวข้อ</li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->

            <!-- แสดงกระทู้ -->
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center">
                    <h3 class="panel-title"> หัวข้อเรื่อง <?php echo $row_view_board['board_topic']; ?> </h3>
                </div>
                <div class="panel-body">
                    <div class="">
                        <strong>หัวข้อเรื่อง <?php echo $row_view_board['board_topic']; ?></strong>
                        <p> โพสต์โดย <?php echo $row_view_board['board_post_by']; ?>
                            วันที่โพสต์ <?php echo date('d/m/Y H:i:s', strtotime($row_view_board['board_date'])); ?>
                        </p>
                        <strong> รายละเอียด </strong>
                        <p><?php echo $row_view_board['board_detail']; ?> </p>
                    </div>
                </div>
            </div>
            <!-- แสดงกระทู้ -->

            <!-- แสดงความคิดเห็น -->
            <div>
                <div>
                    <h4>ความคิดเห็นทั้งหมด</h4>
                </div>
                <?php
                $num = 0;
                while ($comment_board = mysqli_fetch_assoc($res_view_comment_board)) {
                    $num = $num + 1;
                    // echo print_r($comment_board); //เช็คค่า array ที่ส่งมา
                    // exit;
                    ?>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ความคิดเห็นที่ <?php echo $num ?></h3>
                            <!-- <h3 class="panel-title">ความคิดเห็นที่ <?php
                                                                        ?></h3> -->
                        </div>
                        <div class="panel-body">
                            <table>
                                <tbody>
                                    <td width="1000px">
                                        <?php echo $comment_board['board_comment_detail']; ?>
                                    </td>
                                    <td width="250px"> เขียนโดย : <?php echo $comment_board['board_comment_by']; ?></td>
                                    <td width="250px"> <?php echo date('d/m/Y H:i:s', strtotime($comment_board['board_comment_date'])); ?></td>
                                    <?php if ($_SESSION['tbl_profile_fname'] ==  $comment_board['board_comment_by']) { ?>
                                        <td width="150px">
                                            <a class="btn btn-warning btn-xs" href="frm_edit_comment_board.php?board_id=<?= $comment_board['board_id'] ?>&board_comment_id=<?= $comment_board['board_comment_id'] ?>&boardtype_id=<?= $row_view_board['boardtype_id']; ?>">
                                                <span class="glyphicon glyphicon-wrench">แก้ไข</span>
                                            </a>
                                            <a class="btn btn-danger btn-xs" href="del_comment_board.php?id=<?= $comment_board['board_comment_id'] ?>&board_id=<?= $comment_board['board_id'] ?>&boardtype_id=<?= $row_view_board['boardtype_id']; ?>" onclick="return confirm('คุณต้องการที่จะลบข้อมูล\nกดปุ่ม OK เพื่อยืนยัน')">
                                                <span class="glyphicon glyphicon-remove">ลบ</span>
                                            </a>
                                        </td>
                                    <?php }  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php
            }
            ?>
            </div>
            <!-- แสดงความคิดเห็น -->

            <!-- เขียนความคิดเห็น -->
            <div>
                <form id="form1" method="post" action="add_comment_board.php" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
                    <h4>เขียนความคิดเห็น</h4>
                    <textarea type="text" name="news_detail" id="board_comment_detail" class="form-control" required rows="5" cols="50" placeholder="รายละเอียด"></textarea><br>
                    <div align="center">
                        <?php
                        if ($row_view_board['boardtype_id'] == 001) {
                            echo '<a href="web_forum.php?id=001" class="btn btn-info" role="button">
                                        <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                    </a>';
                        } elseif ($row_view_board['boardtype_id'] == 002) {
                            echo '<a href="web_forum.php?id=002" class="btn btn-info" role="button">
                                        <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                    </a>';
                        } elseif ($row_view_board['boardtype_id'] == 003) {
                            echo '<a href="web_forum.php?id=003" class="btn btn-info" role="button">
                                        <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                    </a>';
                        } else {
                            echo '<a href="web_forum.php?id=004" class="btn btn-info" role="button">
                                        <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                    </a>';
                        }
                        ?>
                        <input type="hidden" name="id_board" value="<?php echo $row_view_board['board_id']; ?>">
                        <input type="hidden" name="boardtype_id" value="<?php echo $row_view_board['boardtype_id']; ?>">
                        <input type="hidden" name="comment_by_name" value="<?php echo $_SESSION["tbl_profile_fname"]; ?>">
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;เพิ่มความคิดเห็น</button>
                    </div>
                </form>
            </div><br>
            <!-- เขียนความคิดเห็น -->
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php include('footer.php');  ?>