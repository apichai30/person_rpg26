<?php
session_start();
include('header.php');
include('banner.php');
include('navbar.php');
include('config_db/connectdb.php');

$news_id = $_GET['id'];
$newstype_id = $_GET['type_id'];
$sql = "SELECT * FROM tbl_news WHERE news_id='$news_id'";
$res_news = mysqli_query($dbcon, $sql);
$row_news = mysqli_fetch_array($res_news);
//echo print_r($row_news);
?>
<!-- start body -->
<div class="container">
<div class="row">
        <div class="col-xs-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_news.php">ข่าวสารประชาสัมพันธ์</a></li>
                    <li class="breadcrumb-item active" aria-current="page">แก้ไขรายละเอียดข่าว</li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center">
                    <h3 class="panel-title">แก้ไขรายละเอียดข่าว</h3>
                </div>
                <div class="panel-body">
                    <form id="form1" method="post" action="edit_news.php" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
                        <table width="100" border="10" class="table table-bordered">
                            <tr>
                                <td for="newstype" width="250" align="center">
                                    <h5>ประเภทข่าว</h5>
                                </td>
                                <td colspan="2">
                                    <select class="form-control" name="news_type">
                                        <option value="">กรุณาเลือกประเภทข่าว</option>
                                        <?php
                                        $sql_newstype = "SELECT * FROM tbl_newstype";
                                        $res_newstype = mysqli_query($dbcon, $sql_newstype);
                                        while ($row_newstype = mysqli_fetch_assoc($res_newstype)) {
                                            if ($row_newstype['newstype_id'] == $row_news['newstype_id']) {
                                                echo '<option value="' . $row_newstype['newstype_id'] . '" selected>' . $row_newstype['newstype_detail'] . '</option>'; //
                                            }
                                            echo '<option value="' . $row_newstype['newstype_id'] . '">' . $row_newstype['newstype_detail'] . '</option>';
                                        }
                                        ?>

                                    </select>
                                </td>
                            </tr>
                            <!-- ---------------------------------------------------------------------------------------------------------------- -->
                            <tr>
                                <td for="newstopic" width="250" align="center">
                                    <h5>หัวข้อข่าว</h5>
                                </td>
                                <td colspan="2"><input type="text" name="news_topic" required class="form-control" value="<?php echo $row_news['news_topic']; ?>" placeholder="เขียนหัวข้อข่าว"></td>
                            </tr>
                            <tr>
                                <td for="newsdetail" width="250" align="center">
                                    <h5>เนื้อหาข่าว</h5>
                                </td>
                                <td>
                                    <textarea type="text" name="news_detail" id="news_detail" required class="form-control" rows="10" cols="80" placeholder="เนื้อหาข่าว">
                            <?php echo $row_news["news_detail"]; ?>
                            </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td for="newsstatus" width="200" align="center">
                                    <h5>สถานะข่าว</h5>
                                </td>
                                <td>
                                    <label>
                                        <?php
                                        if ($row_news['news_status'] == 0) {
                                            echo  '<input type="radio" value="0" name="news_status" checked> ข่าวทั่วไป &nbsp';
                                            echo  '&nbsp<input type="radio" value="1" name="news_status"> ข่าวเด่น <br><br>';
                                        } else {
                                            echo  '<input type="radio" value="0" name="news_status"> ข่าวทั่วไป &nbsp';
                                            echo  '&nbsp<input type="radio" value="1" name="news_status" checked> ข่าวเด่น <br><br>';
                                        }
                                        ?>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td for="newsfilename" width="250" align="center">
                                    <h5>เลือกภาพประกอบข่าว</h5>
                                </td>
                                <td colspan="2"><input class="form-control-file" type="file" name="news_filename" accept="image/gif,image/jpeg,image/jpg,image/png"></td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <!-- ตัวอย่างภาพประกอบข่าวเก่า -->
                                    <td>
                                        <label>
                                            <span style="color:red;">
                                                <h5><b>*ตัวอย่างภาพประกอบข่าว (เก่า)</b></h5>
                                            </span>
                                        </label>
                                    </td>
                                </div>
                                <div class="col-md-6">
                                <img src="news_image/<?php echo $row_news['news_filename']; ?>" width="50%" height="auto">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <!-- ตัวอย่างภาพประกอบข่าวใหม่ -->
                                    <td>
                                        <label>
                                            <span id="showtext" style="color:red; text-align:center">
                                            </span>
                                        </label>
                                    </td>
                                </div>
                                <div class="col-md-6">
                                    <img id="showimage" width="50%" height="auto">
                                </div>
                            </div>
                        </div><br>
                        <tr>
                            <div align="center">
                                <input type="hidden" name="news_id" value="<?php echo $row_news['news_id']; ?>">
                                <a href="show_by_newstype.php?id=<?= $newstype_id; ?>" class="btn btn-info" role="button">
                                    <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                </a>
                                <input type="hidden" name="update_by_name" class="form-control" value="<?php echo $_SESSION["tbl_profile_fname"] ?>">
                                <!-- ปุ่ม ค่าเริ่มต้น จะใช้ได้กับinput textarea ใช้ไม่ได้ -->
                                <button type="reset" class="btn btn-warning"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;ค่าเริ่มต้น</button>
                                <button type="submit" class="btn btn-success">
                                    <span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;แก้ไขข่าว
                                </button>
                            </div>
                        </tr>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!-- end body -->
<?php
include('footer.php');
?>