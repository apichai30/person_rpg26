<?php
session_start();
require 'config_db/connectdb.php';
include('header.php');
// include('slide.php');s
include('banner.php');
include('navbar.php');
//echo print_r($row_user); //เช็คค่า array ที่ส่งมา

// $newstype_id = $_GET['id'];

$sql1 = "SELECT tbl_per_info.tbl_per_info_depar ,tbl_profile.tbl_profile_fname ,tbl_profile.tbl_profile_id, tbl_profile.tbl_profile_lname ,tbl_profile.tbl_profile_image
        FROM tbl_per_info INNER JOIN tbl_profile 
        ON tbl_per_info.tbl_profile_id = tbl_profile.tbl_profile_id
        WHERE tbl_per_info.tbl_per_info_depar=1";
$res_profile1 = mysqli_query($dbcon, $sql1);

$sql2 = "SELECT tbl_per_info.tbl_per_info_depar ,tbl_profile.tbl_profile_fname ,tbl_profile.tbl_profile_id, tbl_profile.tbl_profile_lname ,tbl_profile.tbl_profile_image
        FROM tbl_per_info INNER JOIN tbl_profile 
        ON tbl_per_info.tbl_profile_id = tbl_profile.tbl_profile_id
        WHERE tbl_per_info.tbl_per_info_depar=2";
$res_profile2 = mysqli_query($dbcon, $sql2);

$sql3 = "SELECT tbl_per_info.tbl_per_info_depar ,tbl_profile.tbl_profile_fname ,tbl_profile.tbl_profile_id, tbl_profile.tbl_profile_lname ,tbl_profile.tbl_profile_image
        FROM tbl_per_info INNER JOIN tbl_profile 
        ON tbl_per_info.tbl_profile_id = tbl_profile.tbl_profile_id
        WHERE tbl_per_info.tbl_per_info_depar=3";
$res_profile3 = mysqli_query($dbcon, $sql3);

$sql4 = "SELECT tbl_per_info.tbl_per_info_depar ,tbl_profile.tbl_profile_fname ,tbl_profile.tbl_profile_id, tbl_profile.tbl_profile_lname ,tbl_profile.tbl_profile_image
        FROM tbl_per_info INNER JOIN tbl_profile 
        ON tbl_per_info.tbl_profile_id = tbl_profile.tbl_profile_id
        WHERE tbl_per_info.tbl_per_info_depar=4";
$res_profile4 = mysqli_query($dbcon, $sql4);

$sql5 = "SELECT tbl_per_info.tbl_per_info_depar ,tbl_profile.tbl_profile_fname ,tbl_profile.tbl_profile_id, tbl_profile.tbl_profile_lname ,tbl_profile.tbl_profile_image
        FROM tbl_per_info INNER JOIN tbl_profile 
        ON tbl_per_info.tbl_profile_id = tbl_profile.tbl_profile_id
        WHERE tbl_per_info.tbl_per_info_depar=5";
$res_profile5 = mysqli_query($dbcon, $sql5);
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <!-- บอกตำแหน่งที่อยู่ -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item active" aria-current="page">ข้อมูลบุคลกร</li>
                </ol>
            </nav>
            <form method="get" action="search_per.php">
                <div align="right">
                    <div class="form-group" style="width:300px;">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="กรอกชื่อหรือนามสกุล" name="search" required>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-success">ค้นหาข้อมูล</button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="col-lg-12">
                <hr style="border: 1px solid #d6d4d4;">
                <h3 id="depar_1">ฝ่ายบริหารงานโรงเรียน</h3><br>
            </div>
            <!-- แสดงรายการ -->
            <?php while ($row_profile1 = mysqli_fetch_assoc($res_profile1)) { ?>
                <div class="col-md-4">
                    <div class="card" align="center">
                        <img src="profile_image/<?php echo $row_profile1['tbl_profile_image']; ?>" width="60%"><br><br>
                        <p class="card-text"><?php echo $row_profile1['tbl_profile_fname'] . " " . $row_profile1['tbl_profile_lname']; ?></p>
                        <a href="view_per.php?per_id=<?= $row_profile1['tbl_profile_id']; ?>" class="btn btn-info">ดูเพิ่มเติม</a>
                    </div><br><br>
                </div>
            <?php } ?>
            <!-- แสดงรายการ -->

            <div class="col-lg-12">
                <hr style="border: 1px solid #d6d4d4;">
                <h3 id="depar_2">ฝ่ายบริหารงานวิชาการ</h3><br>
            </div>
            <!-- แสดงรายการ -->
            <?php while ($row_profile2 = mysqli_fetch_assoc($res_profile2)) { ?>
                <div class="col-md-4">
                    <div class="card" align="center">
                        <img src="profile_image/<?php echo $row_profile2['tbl_profile_image']; ?>" width="60%"><br><br>
                        <p class="card-text"><?php echo $row_profile2['tbl_profile_fname'] . " " . $row_profile2['tbl_profile_lname']; ?></p>
                        <a href="view_per.php?per_id=<?= $row_profile2['tbl_profile_id']; ?>" class="btn btn-info">ดูเพิ่มเติม</a>
                    </div><br><br>
                </div>
            <?php } ?>
            <!-- แสดงรายการ -->
            <div class="col-lg-12">
                <hr style="border: 1px solid #d6d4d4;">
                <h3 id="depar_3">ฝ่ายบริหารงานงบประมาณ</h3><br>
            </div>
            <!-- แสดงรายการ -->
            <?php while ($row_profile3 = mysqli_fetch_assoc($res_profile3)) { ?>
                <div class="col-md-4">
                    <div class="card" align="center">
                        <img src="profile_image/<?php echo $row_profile3['tbl_profile_image']; ?>" width="60%"><br><br>
                        <p class="card-text"><?php echo $row_profile3['tbl_profile_fname'] . " " . $row_profile3['tbl_profile_lname']; ?></p>
                        <a href="view_per.php?per_id=<?= $row_profile3['tbl_profile_id']; ?>" class="btn btn-info">ดูเพิ่มเติม</a>
                    </div><br><br>
                </div>
            <?php } ?>
            <!-- แสดงรายการ -->
            <div class="col-lg-12">
                <hr style="border: 1px solid #d6d4d4;">
                <h3 id="depar_4">ฝ่ายบริหารงานบุคคล</h3><br>
            </div>
            <!-- แสดงรายการ -->
            <?php while ($row_profile4 = mysqli_fetch_assoc($res_profile4)) { ?>
                <div class="col-md-4">
                    <div class="card" align="center">
                        <img src="profile_image/<?php echo $row_profile4['tbl_profile_image']; ?>" width="60%"><br><br>
                        <p class="card-text"><?php echo $row_profile4['tbl_profile_fname'] . " " . $row_profile4['tbl_profile_lname']; ?></p>
                        <a href="view_per.php?per_id=<?= $row_profile4['tbl_profile_id']; ?>" class="btn btn-info">ดูเพิ่มเติม</a>
                    </div><br><br>
                </div>
            <?php } ?>
            <!-- แสดงรายการ -->

            <div class="col-lg-12">
                <hr style="border: 1px solid #d6d4d4;">
                <h3 id="depar_5">ฝ่ายบริหารงานทั่วไป</h3><br>
            </div>
            <!-- แสดงรายการ -->
            <?php while ($row_profile5 = mysqli_fetch_assoc($res_profile5)) { ?>
                <div class="col-md-4">
                    <div class="card" align="center">
                        <img src="profile_image/<?php echo $row_profile5['tbl_profile_image']; ?>" width="60%"><br><br>
                        <p class="card-text"><?php echo $row_profile5['tbl_profile_fname'] . " " . $row_profile5['tbl_profile_lname']; ?></p>
                        <a href="view_per.php?per_id=<?= $row_profile5['tbl_profile_id']; ?>" class="btn btn-info">ดูเพิ่มเติม</a>
                    </div><br><br>
                </div>
            <?php } ?>
            <!-- แสดงรายการ -->
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php
include('footer.php');
?>