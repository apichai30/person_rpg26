<?php
session_start();
include('header.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';

$row_boardtype_001 = "SELECT * FROM tbl_board WHERE boardtype_id='001' ORDER BY board_id DESC LIMIT 4";
$res_board_001 = mysqli_query($dbcon, $row_boardtype_001);

$row_boardtype_002 = "SELECT * FROM tbl_board WHERE boardtype_id='002' ORDER BY board_id DESC LIMIT 4";
$res_board_002 = mysqli_query($dbcon, $row_boardtype_002);

$row_boardtype_003 = "SELECT * FROM tbl_board WHERE boardtype_id='003' ORDER BY board_id DESC LIMIT 4";
$res_board_003 = mysqli_query($dbcon, $row_boardtype_003);

$row_boardtype_004 = "SELECT * FROM tbl_board WHERE boardtype_id='004' ORDER BY board_id DESC LIMIT 4";
$res_board_004 = mysqli_query($dbcon, $row_boardtype_004);

// echo print_r($res_board_004);
// exit;
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item active" aria-current="page">กระดาน ถาม-ตอบ</li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <h3> กระดาน ถาม-ตอบ ทั้งหมด </h3>
            <div class="panel-group">
                <div class="panel panel-info">
                    <div class="panel-heading" style="color:black;">
                        <h4>พูดคุยเกี่ยวกับเรื่องโรงเรียน&nbsp;&nbsp;
                            <a class="btn btn-primary btn-xs" href="web_forum.php?id=001">ดูทั้งหมด</a>
                        </h4>
                    </div>
                    <?php
                    while ($row_001 = mysqli_fetch_assoc($res_board_001)) {
                        ?>
                        <div class="panel-body">
                            <!-- <?php echo $row_001['board_topic']; ?>
                                                เขียนโดย : <?php echo $row_001['board_post_by']; ?> -->
                            <table>
                                <tbody>
                                    <td width="800px">
                                        <a href="view_topic.php?id=<?= $row_001['board_id']; ?>&board_type=<?= $row_001['boardtype_id']; ?> " style="color:black;">
                                            <?php echo $row_001['board_topic']; ?>
                                        </a>
                                    </td>
                                    <td width="200px"> เขียนโดย : <?php echo $row_001['board_post_by']; ?></td>
                                    <td width="150px"> <?php echo date('d/m/Y H:i:s', strtotime($row_001['board_date'])); ?></td>
                                </tbody>
                            </table>
                        </div>
                    <?php
                }
                ?>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading" style="color:black;">
                        <h4>พูดคุยเกี่ยวกับเรื่องวิชาการ&nbsp;&nbsp;
                            <a class="btn btn-primary btn-xs" href="web_forum.php?id=002">ดูทั้งหมด</a>
                        </h4>
                    </div>
                    <?php
                    while ($row_002 = mysqli_fetch_assoc($res_board_002)) {
                        ?>
                        <div class="panel-body">
                            <table>
                                <tbody>
                                    <td width="800px">
                                        <a href="view_topic.php?id=<?= $row_002['board_id']; ?>&board_type=<?= $row_002['boardtype_id'];?>" style="color:black;">
                                            <?php echo $row_002['board_topic']; ?>
                                        </a>
                                    </td>
                                    <td width="200px"> เขียนโดย : <?php echo $row_002['board_post_by']; ?></td>
                                    <td width="150px"> <?php echo date('d/m/Y H:i:s', strtotime($row_002['board_date'])); ?></td>
                                </tbody>
                            </table>
                        </div>
                    <?php
                }
                ?>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading" style="color:black;">
                        <h4>พูดคุยเกี่ยวกับเรื่องกิจกรรม&nbsp;&nbsp;
                            <a class="btn btn-primary btn-xs" href="web_forum.php?id=003">ดูทั้งหมด</a>
                        </h4>
                    </div>
                    <?php
                    while ($row_003 = mysqli_fetch_assoc($res_board_003)) {
                        ?>
                        <div class="panel-body">
                            <table>
                                <tbody>
                                    <td width="800px">
                                        <a href="view_topic.php?id=<?= $row_003['board_id']; ?> &board_type=<?= $row_003['boardtype_id'];?>" style="color:black;">
                                            <?php echo $row_003['board_topic']; ?>
                                        </a>
                                    </td>
                                    <td width="200px"> เขียนโดย : <?php echo $row_003['board_post_by']; ?></td>
                                    <td width="150px"> <?php echo date('d/m/Y H:i:s', strtotime($row_003['board_date'])); ?></td>
                                </tbody>
                            </table>
                        </div>
                    <?php
                }
                ?>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading" style="color:black;">
                        <h4>พูดคุยเกี่ยวกับเรื่องทั่วไป&nbsp;&nbsp;
                            <a class="btn btn-primary btn-xs" href="web_forum.php?id=004">ดูทั้งหมด</a>
                        </h4>
                    </div>
                    <?php
                    while ($row_004 = mysqli_fetch_assoc($res_board_004)) {
                        ?>
                        <div class="panel-body">
                            <table>
                                <tbody>
                                    <td width="800px">
                                        <a href="view_topic.php?id=<?= $row_004['board_id']; ?> &board_type=<?= $row_004['boardtype_id'];?>" style="color:black;">
                                            <?php echo $row_004['board_topic']; ?>
                                        </a>
                                    </td>
                                    <td width="200px"> เขียนโดย : <?php echo $row_004['board_post_by']; ?></td>
                                    <td width="150px"> <?php echo date('d/m/Y H:i:s', strtotime($row_004['board_date'])); ?></td>
                                </tbody>
                            </table>
                        </div>
                    <?php
                }
                ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>

<?php include('footer.php');  ?>