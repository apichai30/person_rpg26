<?php
session_start();
if (isset($_SESSION['is_member']) or isset($_SESSION['is_admin']) or isset($_SESSION['is_news'])) {
} else {
    echo ("<script>
        window.alert('กรุณาเข้าสู่ระบบก่อนทำรายการ');
        window.location.href='login_system/index.php';
        </script>");
}
include('header.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';

$per_id = $_GET['per_id'];
// $sql = "SELECT * FROM tbl_profile WHERE tbl_profile_id='$per_id'";

$sql = "SELECT pro.*,info.*
FROM tbl_profile AS pro 
INNER JOIN tbl_per_info AS info ON pro.tbl_profile_id = info.tbl_profile_id
WHERE pro.tbl_profile_id='$per_id'";

// echo $sql;
// exit;

$res_per = mysqli_query($dbcon, $sql);
$row_per = mysqli_fetch_array($res_per);

// echo print_r($row_per); //เช็คค่า array ที่ส่งมา
// exit;
?>
<div class="container">
    <div class="row">
        <!-- <div class="col-md-12 ">  col-md-offset-2 คือการเว้นช่วงไป 2 คอลัม -->
        <!-- บอกตำแหน่งที่อยู่ -->
        <div class="col-md-6 col-lg-8 col-md-offset-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_per.php">ข้อมูลบุคลากร</a></li>
                    <li class="breadcrumb-item active" aria-current="page">ดูข้อมูลบุคลากร</li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center">
                    <h3 class="panel-title">ข้อมูลบุคลากร</h3>
                </div>
                <div class="panel-body table-responsive">
                    <form method="post" accept-charset="UTF-8" role="form">
                        <fieldset>
                            <div style="text-align: center">
                                <h4>ประวัติส่วนตัว</h4>
                            </div>
                            <!-- แสดงรูปประจำตัว -->
                            <div class="form-group" align="center">
                                <div><img src="profile_image/<?php echo $row_per['tbl_profile_image']; ?>" width="200px" height="auto"><br></div>
                            </div>
                            <table width="100" border="10" class="table table-bordered">
                                <tr>
                                    <td width="200" align="center">
                                        <h5>ชื่อ-สกุล</h5>
                                    </td>
                                    <td colspan="2">
                                        <h5>
                                            <?php
                                            if ($row_per['tbl_profile_prefix'] == 1) {
                                                echo  '<input type="hidden" value="1" name="tbl_profile_prefix">นาย &nbsp';
                                            } elseif ($row_per['tbl_profile_prefix'] == 2) {
                                                echo  '<input type="hidden" value="2" name="tbl_profile_prefix">นาง &nbsp';
                                            } else {
                                                echo  '<input type="hidden" value="3" name="tbl_profile_prefix">นางสาว &nbsp';
                                            }
                                            echo $row_per['tbl_profile_fname'] . " " . $row_per['tbl_profile_lname'];
                                            ?>
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200" align="center">
                                        <h5>เพศ</h5>
                                    </td>
                                    <td colspan="2">
                                        <h5>
                                            <p>
                                                <?php
                                                if ($row_per['tbl_profile_gender'] == 1) {
                                                    echo  '<input type="hidden" value="1" name="tbl_profile_prefix">ชาย';
                                                } else {
                                                    echo  '<input type="hidden" value="2" name="tbl_profile_prefix">หญิง';
                                                }
                                                ?>
                                            </p>
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200" align="center">
                                        <h5>วัน/เดือน/ปี เกิด</h5>
                                    </td>
                                    <td colspan="2">
                                        <h5>
                                            <?php echo date('d/m/Y', strtotime($row_per['tbl_profile_birth'])); ?>
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200" align="center">
                                        <h5>ที่อยู่อาศัย</h5>
                                    </td>
                                    <td colspan="2">
                                        <h5>
                                            <?php echo $row_per['tbl_profile_address']; ?>
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200" align="center">
                                        <h5>รหัสไปรษณีย์</h5>
                                    </td>
                                    <td colspan="2">
                                        <h5>
                                            <?php echo $row_per['tbl_profile_zipcode']; ?>
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200" align="center">
                                        <h5>อีเมล์</h5>
                                    </td>
                                    <td colspan="2">
                                        <h5>
                                            <?php echo $row_per['tbl_profile_email']; ?>
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200" align="center">
                                        <h5>เบอร์โทรศัพท์</h5>
                                    </td>
                                    <td colspan="2">
                                        <h5>
                                            <?php echo $row_per['tbl_profile_phone']; ?>
                                        </h5>
                                    </td>
                                </tr>
                            </table>
                            <div style="text-align: center">
                                <h4>ข้อมูลราชการ</h4>
                            </div>
                            <table width="100" border="10" class="table table-bordered">
                            <tr>
                                <td width="200" align="center">
                                    <h5>ตำแหน่ง</h5>
                                </td>
                                <td>
                                    <h5>
                                    <?php
                                    if ($row_per['tbl_per_info_rank'] == 1) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_rank" value="" readonly>ผู้อำนวยการโรงเรียน';
                                    } elseif ($row_per['tbl_per_info_rank'] == 2) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_rank" value="" readonly>รองผู้อำนวยการโรงเรียน';
                                    } elseif ($row_per['tbl_per_info_rank'] == 3) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_rank" value="" readonly>หัวหน้าฝ่ายงาน';
                                    } elseif ($row_per['tbl_per_info_rank'] == 4) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_rank" value="" readonly>ครูประจำวิชา';
                                    } elseif ($row_per['tbl_per_info_rank'] == 5) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_rank" value="" readonly>ครูพิเศษ';
                                    } elseif ($row_per['tbl_per_info_rank'] == 6) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_rank" value="" readonly>พนักงานทั่วไป';
                                    } elseif ($row_per['tbl_per_info_rank'] == 7) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_rank" value="" readonly>นักศึกษาฝึกประสบการณ์';
                                    } else {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_rank" value="" readonly>ไม่ทราบข้อมูล';
                                    }
                                    ?>
                                    </h5>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>วิชาสอน</h5>
                                </td>
                                <td>
                                    <h5>
                                    <?php
                                    if ($row_per['tbl_per_info_subjects'] == 1) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>ภาษาไทย';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 2) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>วิทยาศาสตร์';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 3) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>คณิตศาสตร์';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 4) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>สังคมศึกษา';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 5) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>ภาษาต่างประเทศ';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 6) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>การงานอาชีพและเทคโนโลยี';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 7) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>สุขศึกษาและพลศึกษา';
                                    } elseif ($row_per['tbl_per_info_subjects'] == 8) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>ศิลปะ';
                                    } else {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_subjects" value="" readonly>ไม่ทราบข้อมูล';
                                    }
                                    ?>
                                    </h5>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>สถานะ</h5>
                                </td>
                                <td>
                                    <h5>
                                    <?php
                                    if ($row_per['tbl_per_info_cate'] == 1) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_cate" value="" readonly>ข้าราชการครู';
                                    } elseif ($row_per['tbl_per_info_cate'] == 2) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_cate" value="" readonly>ข้าราชการทั่วไป';
                                    } else {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_cate" value="" readonly>ไม่ทราบข้อมูล';
                                    }
                                    ?>
                                    </h5>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>ฝ่ายงาน</h5>
                                </td>
                                <td>
                                    <h5>
                                    <?php
                                    if ($row_per['tbl_per_info_depar'] == 1) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_depar" value="" readonly>บริหารงานโรงเรียน';
                                    } elseif ($row_per['tbl_per_info_depar'] == 2) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_depar" value="" readonly>บริหารงานวิชาการ';
                                    } elseif ($row_per['tbl_per_info_depar'] == 3) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_depar" value="" readonly>บริหารงานงบประมาณ';
                                    } elseif ($row_per['tbl_per_info_depar'] == 4) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_depar" value="" readonly>บริหารงานบุคคล';
                                    } elseif ($row_per['tbl_per_info_depar'] == 5) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_depar" value="" readonly>บริหารงานทั่วไป';
                                    } else {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_depar" value="" readonly>ไม่ทราบข้อมูล';
                                    }
                                    ?>
                                    </h5>
                                </td>
                            </tr>
                            <tr>
                                <td width="200" align="center">
                                    <h5>การศึกษาสูงสุด</h5>
                                </td>
                                <td>
                                    <h5>
                                    <?php
                                    if ($row_per['tbl_per_info_educa'] == 1) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_educa" value="" readonly>จบการศึกษาขั้นพื้นฐาน';
                                    } elseif ($row_per['tbl_per_info_educa'] == 2) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_educa" value="" readonly>จบมัธยมศึกษาปีที่ 6';
                                    } elseif ($row_per['tbl_per_info_educa'] == 3) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_educa" value="" readonly>จบประกาศนียบัตรวิชาชีพ';
                                    } elseif ($row_per['tbl_per_info_educa'] == 4) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_educa" value="" readonly>จบประกาศนียบัตรวิชาชีพชั้นสูง';
                                    } elseif ($row_per['tbl_per_info_educa'] == 5) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_educa" value="" readonly>จบปริญญาตรี';
                                    } elseif ($row_per['tbl_per_info_educa'] == 6) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_educa" value="" readonly>จบปริญญาโท';
                                    } elseif ($row_per['tbl_per_info_educa'] == 7) {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_educa" value="" readonly>จบปริญญาเอก';
                                    } else {
                                        echo  '<input type="hidden" class="form-control" name="tbl_per_info_educa" value="" readonly>ไม่ทราบข้อมูล';
                                    }
                                    ?>
                                    </h5>
                                </td>
                            </tr>
                            </table>
                        </fieldset>
                        <tr>
                            <div align="center">
                                <a href="index_per.php" class="btn btn-info" role="button">
                                    <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                </a>
                                <!-- <button type="reset" class="btn btn-warning"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;ค่าเริ่มต้น</button>
								<button type="submit" class="btn btn-success" onclick="return confirm('คุณต้องการที่จะแก้ไขข้อมูล\nกดปุ่ม OK เพื่อยืนยัน')">
                                    <span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;บันทึก
                                </button> -->
                            </div>
                        </tr>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php include('footer.php');  ?>