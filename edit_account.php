<?php
include('header.php');
require 'config_db/connectdb.php';
$per_id = $_POST['per_id'];

// tbl_login
// $login_username = $_POST['username'];
$login_password = $_POST['new_password'];

// tbl_profile
$tbl_profile_prefix = $_POST['profile_prefix'];
$tbl_profile_fname = $_POST['profile_fname'];
$tbl_profile_lname = $_POST['profile_lname'];
$tbl_profile_gender = $_POST['profile_gender'];
$tbl_profile_birth = $_POST['profile_birth'];
$tbl_profile_address = $_POST['profile_address'];
$tbl_profile_zipcode = $_POST['profile_zipcode'];
$tbl_profile_email = $_POST['profile_email'];
$tbl_profile_phone = $_POST['profile_phone'];

// echo '<pre>';
// print_r($_POST); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// echo exit();

// if($_POST["std_pass"] != $_POST["conpassword"])
// {
// echo "Password not Match!";
// exit();
// }

// $check_user = "SELECT * FROM tbl_login  WHERE login_username = '$login_username'";
// $res1 = mysqli_query($dbcon, $check_user) or die(mysqli_error($dbcon));
// $num_user = mysqli_fetch_array($res1);
// //แก้ไขว่าถ้าไม่เปลี่ยน user ให้ไปทำ else
// if (strcmp($num_user,$login_username)==0) {
//     //ถ้ามี username นี้อยู่ในระบบแล้วให้แจ้งเตือน
//     echo '<script>';
//     echo 'alert(" ชื่อผู้ใช้ นี้ถูกใช้งานแล้ว กรุณาสมัครใหม่อีกครั้ง !");';
//     // echo 'window.location="account.php";';
//     echo '</script>';
// }
//     $check_email = "SELECT * FROM tbl_profile  WHERE tbl_profile_email = '$tbl_profile_email'";
//     $res2 = mysqli_query($dbcon, $check_email) or die(mysqli_error($dbcon));
//     $num_email = mysqli_num_rows($res2);
//     if (strcmp($num_email,$tbl_profile_email)==0) {
//         //ถ้ามี email นี้อยู่ในระบบแล้วให้แจ้งเตือน
//         echo '<script>';
//         echo 'alert(" อีเมล์ นี้ถูกใช้งานแล้ว กรุณาสมัครใหม่อีกครั้ง !");';
//         // echo 'window.location="account.php";';
//         echo '</script>';
//     }
// echo '<pre>';
// print_r($_POST); //เช็คค่า array ที่ส่งมา
// echo '</pre>';
// exit();

// upload image
if ($_FILES['profile_image']['size'] > 0 && $_FILES['profile_image']['error'] == 0) {
    //delete old image
    $sql_select = "SELECT tbl_profile_image FROM tbl_profile WHERE tbl_profile_id='$per_id'";
    $result_image = mysqli_query($dbcon, $sql_select);
    $row_image = mysqli_fetch_assoc($result_image);
    $image_old = $row_image['tbl_profile_image'];
    unlink("profile_image/" . $image_old);

    //upload new image
    $image_ext = pathinfo(basename($_FILES['profile_image']['name']), PATHINFO_EXTENSION);
    $new_image_name = 'img_profile_' . uniqid() . "." . $image_ext;
    $image_path = "profile_image/";
    $image_upload_path = $image_path . $new_image_name;
    $success = move_uploaded_file($_FILES['profile_image']['tmp_name'], $image_upload_path);
    //insert image to db
    // $sql_image = "UPDATE tbl_profile SET tbl_profile_image='$new_image_name' WHERE tbl_profile_id='$per_id'";
    // mysqli_query($dbcon, $sql_image);

    $sql = "UPDATE tbl_profile SET tbl_profile_prefix='$tbl_profile_prefix', tbl_profile_fname='$tbl_profile_fname', tbl_profile_lname='$tbl_profile_lname',
                                tbl_profile_gender='$tbl_profile_gender', tbl_profile_birth='$tbl_profile_birth', tbl_profile_address='$tbl_profile_address',
                                tbl_profile_zipcode='$tbl_profile_zipcode', tbl_profile_email='$tbl_profile_email', tbl_profile_phone='$tbl_profile_phone',
                                tbl_profile_image='$new_image_name'
        WHERE (tbl_profile_id='$per_id')";
} else {
    $sql = "UPDATE tbl_profile SET tbl_profile_prefix='$tbl_profile_prefix', tbl_profile_fname='$tbl_profile_fname', tbl_profile_lname='$tbl_profile_lname',
                                tbl_profile_gender='$tbl_profile_gender', tbl_profile_birth='$tbl_profile_birth', tbl_profile_address='$tbl_profile_address',
                                tbl_profile_zipcode='$tbl_profile_zipcode', tbl_profile_email='$tbl_profile_email', tbl_profile_phone='$tbl_profile_phone',
                                tbl_profile_image='user_default.png'
        WHERE (tbl_profile_id='$per_id')";
} //user_default.png

//เข้ารหัส password
// $salt = 'tikde78uj4ujuhlaoikiksakeidke';
// $hash_login_password = hash_hmac('sha256', $login_password, $salt);

// $sql2 = "INSERT INTO tbl_login (tbl_profile_id, login_password) 
//                     VALUES ((SELECT MAX(tbl_profile_id) FROM tbl_profile), '$hash_login_password')";

$result = mysqli_query($dbcon, $sql);
// $result2 = mysqli_query($dbcon, $sql2);


if ($result) {
    // go to account.php
    header("Location: account.php");
} else {
    // echo '<script>';
    // echo 'alert(" เกิดข้อผิดพลาด กรุณาลงทะเบียนใหม่ในภายหลัง ! ");' . mysqli_error($dbcon);
    // echo 'window.location="index.php";';
    // echo '</script>';
    echo mysqli_error($dbcon);
}

mysqli_close($dbcon);
?>