<?php
//echo '<pre>';
//print_r($row_user); //เช็คค่า array ที่ส่งมา
//echo '</pre>';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ระบบข้อมูลบุคลากรและผลงาน</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="css/datepicker.css" rel="stylesheet">

  <script src="js/jquery.min.js"></script>

  <!-- start datatable -->
  <script>
    $(document).ready(function() {
      $('#table_news').DataTable({
        "aaSorting": [
          [0, 'desc']
        ],
        "oLanguage": {
          "sLengthMenu": "แสดง _MENU_ จำนวน ต่อหน้า",
          "sZeroRecords": "ไม่พบข้อมูล",
          "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ จำนวน",
          "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 จำนวน",
          "sInfoFiltered": "(จากจำนวนทั้งหมด _MAX_ จำนวน)",
          "sSearch": "ค้นหา : ",
          "aaSorting": [
            [0, 'desc']
          ],
          "oPaginate": {
            "sFirst": "หน้าแรก",
            "sPrevious": "ก่อนหน้า",
            "sNext": "ถัดไป",
            "sLast": "หน้าสุดท้าย"
          },
        }
      });
    });
  </script>
  <!-- end datatable -->
  <style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Kanit&display=swap');

    body {
      /* background: url('img/bg.jpg') !important; */
      font-family: 'Kanit', sans-serif;
    }

    .affix {
      top: 0;
      width: 98%;
      z-index: 9999 !important;
    }

    .affix+.container-fluid {
      padding-top: 70px;
    }

    #toTop {
      padding: 10px 10px;
      border-radius: 4px;
      background: #FF9800;
      color: #fff;
      position: fixed;
      bottom: 100px;
      right: 19px;
      display: none;
    }

    #toTop:hover {
      cursor: pointer;
      background-color: #333;
    }

    /* #banner {
      height: 200px;
      width: 100%;
    } */

    .clear {
      margin-bottom: 12rem;
    }
  </style>
</head>

<body>