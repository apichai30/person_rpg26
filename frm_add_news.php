<?php
session_start();
include('header.php');
include('banner.php');
include('navbar.php');
require 'config_db/connectdb.php';

$newstype_id = $_GET['id'];
// echo print_r($_GET);
?>
<!-- start body -->
<div class="container">
<div class="row">
        <div class="col-xs-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href="index_news.php">ข่าวสารประชาสัมพันธ์</a></li>
                    <li class="breadcrumb-item active" aria-current="page">เพิ่มข่าว</li>
                </ol>
            </nav>
            <!-- บอกตำแหน่งที่อยู่ -->
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center">
                    <h3 class="panel-title">เพิ่มข่าว</h3>
                </div>
                <div class="panel-body">
                    <form id="form1" method="post" action="add_news.php?id=<?php echo $newstype_id; ?>" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
                        <table width="100" border="10" class="table table-bordered">
                            <tr>
                                <td for="newstype" width="250" align="center">
                                    <h5>ประเภทข่าว</h5>
                                </td>
                                <td colspan="2">
                                    <?php
                                    if ($newstype_id == 001) {
                                        echo  '<input type="text" class="form-control" name="news_type" value="ข่าวสารที่น่าสนใจ" readonly>';
                                    }elseif ($newstype_id == 002) {
                                        echo  '<input type="text" class="form-control" name="news_type" value="ข่าวสารแจ้งการประชุม" readonly>';
                                    } elseif ($newstype_id == 003) {
                                        echo  '<input type="text" class="form-control" name="news_type" value="ข่าวสารแจ้งกิจกรรม" readonly>';
                                    } elseif ($newstype_id == 004) {
                                        echo  '<input type="text" class="form-control" name="news_type" value="ข่าวสารแจ้งวันหยุด" readonly>';
                                    } elseif ($newstype_id == 005) {
                                        echo  '<input type="text" class="form-control" name="news_type" value="ข่าวสารอื่นๆ" readonly>';
                                    } else {
                                        echo  '<input type="text" class="form-control" name="news_type" value="ข่าวเด่น" readonly>';
                                    }
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <td for="newstopic" width="250" align="center">
                                    <h5>หัวข้อข่าว</h5>
                                </td>
                                <td colspan="2"><input type="text" name="news_topic" required class="form-control" placeholder="เขียนหัวข้อข่าว"></td>
                            </tr>
                            <!-- -------------------------------------------------------------------------------------------------------- -->
                            <tr>
                                <td for="newsdetail" width="250" align="center">
                                    <h5>เนื้อหาข่าว</h5>
                                </td>
                                <td>
                                    <textarea type="text" name="news_detail" id="news_detail" required class="form-control" rows="10" cols="80" placeholder="เนื้อหาข่าว"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td for="newsstatus" width="200" align="center">
                                    <h5>สถานะข่าว</h5>
                                </td>
                                <td>
                                    <label><input type="radio" name="news_status" required value="0" checked> ข่าวทั่วไป</label>&nbsp;&nbsp;
                                    <label><input type="radio" name="news_status" required value="1"> ข่าวเด่น</label>
                                </td>
                            </tr>
                            <tr>
                                <td for="newsfilename" width="250" align="center">
                                    <h5>เลือกภาพประกอบข่าว</h5>
                                </td>
                                <td colspan="2"><input class="form-control-file" type="file" name="news_filename" accept="image/gif,image/jpeg,image/jpg,image/png"></td>
                            </tr>
                        </table>
                        <tr>
                            <div align="center">
                                <a href="show_by_newstype.php?id=<?= $newstype_id; ?>" class="btn btn-info" role="button">
                                    <span class="glyphicon glyphicon-repeat"></span>&nbsp;กลับ
                                </a>
                                <input type="hidden" name="add_by_name" class="form-control" value="<?php echo $_SESSION["tbl_profile_fname"] ?>">
                                <button type="reset" class="btn btn-warning"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;ค่าเริ่มต้น</button>
                                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;เพิ่มข่าว</button>
                            </div>
                        </tr>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!-- end body -->
<?php
include('footer.php');
?>